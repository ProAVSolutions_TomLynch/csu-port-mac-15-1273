PROGRAM_NAME = 'CSU Port Mac Config'
#DEFINE CSU_CONFIG


DEFINE_CONSTANT
// NI INTERNAL dvRELAYS
AMP_POWER			= 99
SCRN_1_UP			= 99
SCRN_1_DN			= 99
SCRN_2_UP			= 99
SCRN_2_DN			= 99
SCRN_3_UP			= 99
SCRN_3_DN			= 99
CEILING_MIC 	= 99
SCRN_4_UP			= 99
SCRN_4_DN			= 99
ROOM_JOIN			= 99

//Blind Control Relays
BLND_1_UP = 99
BLND_1_DN = 99
BLND_2_UP = 99
BLND_2_DN = 99
BLND_3_UP = 99
BLND_3_DN = 99

// NI INTERNAL IO's
PIR           = 1
PC_STATUS_IO	= 4

//DVX INPUTS

VIN_LT				= 3
VIN_PC				= 4
VIN_DOC_CAM		= 5

AIN_JOIN			= 99
VIN_JOIN			= 99
VIN_JOIN_2		= 99
VIN_LT_2			= 99
VIN_MICRO_1		= 99
VIN_MICRO_2		= 99
VIN_AUX_AV		= 99
VIN_HDMI			= 99
VIN_VC1				= 99
VIN_VC2				= 99
VIN_DVD				= 99
VIN_SPARE1		= 99
VIN_SPARE2		= 99
VIN_SPARE3		= 99

//DVX OUTPUTS
VOUT_DISPLAY1	= 1
VOUT_DISPLAY2	= 2
VOUT_DISPLAY3	= 99
VOUT_VC				= 99
//VOUT_TP		= 1

AOUT_INT_AMP	= 1
AOUT_EXT_AMP	= 2
AOUT_VC				= 3

SOURCE_SELECT	= 1

MET_7_BTN_LAPTOP_1 	= 99
MET_7_BTN_LAPTOP_2 	= 99
MET_7_BTN_PC 		 		= 99
MET_7_BTN_DOC_CAM		= 99
MET_7_BTN_MICRO_1 	= 99
MET_7_BTN_MICRO_2		= 99
MET_7_BTN_VC				= 99
MET_7_BTN_MUTE	 		= 99
MET_7_BTN_OFF		 		= 99
MET_7_BTN_VOL_DOWN	= 99
MET_7_BTN_VOL_UP 		= 99

DEFINE_VARIABLE

VOLATILE CHAR LECTOPIA_VENUE[] = 'CSU Port Macquarie Bld 801 Rm 1026'
//VOLATILE CHAR LECTOPIA_IP[] = '10.1.1.17'

VOLATILE CHAR TOUCH_PANEL_MENU[20] = '' //Standard Space
//VOLATILE CHAR TOUCH_PANEL_MENU[20] = '_FOS'
//VOLATILE CHAR TOUCH_PANEL_MENU[20] = '_IVT'
//VOLATILE CHAR TOUCH_PANEL_MENU[20] = '_SECONDARY_IVT'
//VOLATILE CHAR TOUCH_PANEL_MENU[20] = '_SECONDARY_IVT_VC'

PERSISTENT CHAR RMS_DISPLAY_MODULE[3][20] = {'SONY_VP'			, '' , ''}
PERSISTENT CHAR RMS_DISPLAY_TYPE[3][20]		= {'Projector 1'  , '' , ''}
PERSISTENT CHAR RMS_DISPLAY_BRAND[3][20]	= {'Sony'					, '' , ''}
PERSISTENT CHAR RMS_DISPLAY_MODEL[3][20]	= {'VPL-FHZ55'		, '' , ''}
PERSISTENT CHAR RMS_DISPLAY_RGBHV[3][5]		= {'R2'						, '' , ''}
PERSISTENT CHAR RMS_DISPLAY_HDMI[3][5]		= {'R4'						, '' , ''}


// DVD
VOLATILE CHAR RMS_DVD_TYPE[]  = ''
VOLATILE CHAR RMS_DVD_BRAND[] = ''
VOLATILE CHAR RMS_DVD_MODEL[] = ''

// TOUCHSCREEN
VOLATILE CHAR RMS_TP_TYPE[] = 'Touch Screen'
VOLATILE CHAR RMS_TP_MODEL[] = 'MST-701'

//VIDEO CONFERENCE
//#DEFINE HDX
//VOLATILE CHAR RMS_VC_MODEL[] = 'HDX-8000'
//#DEFINE GROUP_700
//VOLATILE CHAR RMS_VC_MODEL[] = 'Group 700'
//VOLATILE CHAR RMS_VC_MODEL[] = 'Group 500'

// AUDIO DSP
//#DEFINE TESIRA
//VOLATILE CHAR RMS_DSP_TYPE[] = 'Audio DSP'
//VOLATILE CHAR RMS_DSP_BRAND[] = 'Biamp'
//VOLATILE CHAR RMS_DSP_MODEL[] = 'Tesira Forte CL'
//VOLATILE CHAR RMS_DSP_MODEL[] = 'Nexia VC'

// VGA SWITCHER
VOLATILE CHAR RMS_SWITCHER_TYPE[] = 'Enova DVX'
VOLATILE CHAR RMS_SWITCHER_BRAND[] = 'AMX'
#warn 'check dvx number'
VOLATILE CHAR RMS_SWITCHER_MODEL[] = 'DVX-2250'

//VOLATILE CHAR RMS_SWITCHER_MODEL[] = 'DVX-3155'
//VOLATILE CHAR RMS_SWITCHER_MODEL[] = 'DVX-3150'
//VOLATILE CHAR RMS_SWITCHER_MODEL[] = 'DVX-2155'

#IF_NOT_DEFINED CSU_MAIN
	#INCLUDE 'CSU Port Mac Main v0.01.axs'
#END_IF

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)
