(*********************************************************************)
(*                                                                   *)
(*             AMX Resource Management Suite  (3.3.33)               *)
(*                                                                   *)
(*********************************************************************)
/*
 *  Legal Notice :
 * 
 *     Copyright, AMX LLC, 2009
 *
 *     Private, proprietary information, the sole property of AMX LLC.  The
 *     contents, ideas, and concepts expressed herein are not to be disclosed
 *     except within the confines of a confidential relationship and only
 *     then on a need to know basis.
 * 
 *     Any entity in possession of this AMX Software shall not, and shall not
 *     permit any other person to, disclose, display, loan, publish, transfer
 *     (whether by sale, assignment, exchange, gift, operation of law or
 *     otherwise), license, sublicense, copy, or otherwise disseminate this
 *     AMX Software.
 * 
 *     This AMX Software is owned by AMX and is protected by United States
 *     copyright laws, patent laws, international treaty provisions, and/or
 *     state of Texas trade secret laws.
 * 
 *     Portions of this AMX Software may, from time to time, include
 *     pre-release code and such code may not be at the level of performance,
 *     compatibility and functionality of the final code. The pre-release code
 *     may not operate correctly and may be substantially modified prior to
 *     final release or certain features may not be generally released. AMX is
 *     not obligated to make or support any pre-release code. All pre-release
 *     code is provided "as is" with no warranties.
 * 
 *     This AMX Software is provided with restricted rights. Use, duplication,
 *     or disclosure by the Government is subject to restrictions as set forth
 *     in subparagraph (1)(ii) of The Rights in Technical Data and Computer
 *     Software clause at DFARS 252.227-7013 or subparagraphs (1) and (2) of
 *     the Commercial Computer Software Restricted Rights at 48 CFR 52.227-19,
 *     as applicable.
*/

PROGRAM_NAME='RMSTimer'
(*{{PS_SOURCE_INFO(PROGRAM STATS)                          *)
(***********************************************************)
(*  ORPHAN_FILE_PLATFORM: 1                                *)
(***********************************************************)
(*}}PS_SOURCE_INFO                                         *)
(***********************************************************)
#IF_NOT_DEFINED __RMS_TIMER__
#DEFINE __RMS_TIMER__

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT

(* Version For Code *)
CHAR __RMS_TIMER_NAME__[]       = 'RMSTimer.axi'
CHAR __RMS_TIMER_VERSION__[]    = '3.3.33'

// Timeline ID
#IF_NOT_DEFINED TL_RMS_TIMER
TL_RMS_TIMER                 = 1
#END_IF

// Callback periods
RMS_TIMER_CB_MINUTE          = 0
RMS_TIMER_CB_HOUR            = 1
RMS_TIMER_CB_DAY             = 2
RMS_TIMER_CB_MONTH           = 3
RMS_TIMER_CB_YEAR            = 4

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

#IF_NOT_DEFINED alTLTimer
VOLATILE LONG alTLTimer[] = { 60000 }    // 60 seconds
#END_IF

#IF_NOT_DEFINED bRMSDebug
VOLATILE CHAR bRMSDebug = 0
#END_IF

// Vars
VOLATILE CHAR cRMSRefTime[20]
VOLATILE CHAR cRMSRefDate[20]
VOLATILE LONG lRMSMinutes
VOLATILE LONG lRMSHours
VOLATILE LONG lRMSSeconds
VOLATILE LONG lRMSDays
VOLATILE LONG lRMSMonths
VOLATILE LONG lRMSYears
VOLATILE LONG lRMSLastTimeValue
VOLATILE INTEGER nRMSCallbackTime

(***********************************************************)
(*           SUBROUTINE DEFINITIONS GO BELOW               *)
(***********************************************************)

(**********************************************************)
(* Function: RMSTimerSetCallbackTime                       *)
(* Purpose:  Set Callback Time                            *)
(**********************************************************)
DEFINE_FUNCTION INTEGER RMSTimerSetCallbackTime(INTEGER nCBTime)
{
  IF (nCBTime <= RMS_TIMER_CB_YEAR)
    nRMSCallbackTime = nCBTime
  RETURN nRMSCallbackTime;
}

(**********************************************************)
(* Function: RMSTimerGetCallbackTime                      *)
(* Purpose:  Get Callback Time                            *)
(**********************************************************)
DEFINE_FUNCTION INTEGER RMSTimerGetCallbackTime(INTEGER nCBTime)
{
  RETURN nRMSCallbackTime;
}

(**********************************************************)
(* Function: RMSTimerStart                                *)
(* Purpose:  Start Timer                                  *)
(**********************************************************)
DEFINE_FUNCTION SLONG RMSTimerStart()
{
  // Running?
  IF (TIMELINE_ACTIVE(TL_RMS_TIMER))
    RETURN -1;

  // Start Timeline
  TIMELINE_CREATE(TL_RMS_TIMER,alTLTimer,MAX_LENGTH_ARRAY(alTLTimer),TIMELINE_RELATIVE,TIMELINE_REPEAT)
  TIMELINE_SET(TL_RMS_TIMER,alTLTimer[MAX_LENGTH_ARRAY(alTLTimer)]-100)
  cRMSRefTime = TIME
  cRMSRefDate = LDATE
  lRMSMinutes = 0
  lRMSHours = 0
  lRMSSeconds = 0
  lRMSDays = 0
  lRMSMonths = 0
  lRMSYears = 0
  IF (bRMSDebug)
    SEND_STRING 0,"'RMS Timer: Start timer'"
}

(**********************************************************)
(* Function: RMSTimerStop                                  *)
(* Purpose:  Stop Timer                                   *)
(**********************************************************)
DEFINE_FUNCTION SLONG RMSTimerStop()
{
  // Stopped?
  IF (!TIMELINE_ACTIVE(TL_RMS_TIMER))
    RETURN -1;

  lRMSLastTimeValue = TIMELINE_GET(TL_RMS_TIMER)
  TIMELINE_KILL(TL_RMS_TIMER)
  IF (bRMSDebug)
    SEND_STRING 0,"'RMS Timer: Stop timer'"
}

(**********************************************************)
(* Function: RMSTimerIsRunning                             *)
(* Purpose:  Well, is it?                                 *)
(**********************************************************)
DEFINE_FUNCTION CHAR RMSTimerIsRunning()
{
  RETURN TIMELINE_ACTIVE(TL_RMS_TIMER);
}

(**********************************************************)
(* Function: RMSTimerResetStartTime                        *)
(* Purpose:  Artificially move reference time             *)
(**********************************************************)
DEFINE_FUNCTION CHAR[10] RMSTimerResetStartTime(SLONG slSeconds, SLONG slMinutes, SLONG slHours)
STACK_VAR
SLONG slDayOffset
{
  cRMSRefTime = RMSTimerTimeAdd(cRMSRefTime, slSeconds, slMinutes, slHours, slDayOffset)
  IF (bRMSDebug)
    SEND_STRING 0,"'RMS Timer: Reset Start Time to ',cRMSRefTime"
  RETURN cRMSRefTime;
}

(**********************************************************)
(* Function: RMSTimerGetSeconds                            *)
(* Purpose:  Get Seconds                                  *)
(**********************************************************)
DEFINE_FUNCTION LONG RMSTimerGetSeconds()
{
  // Get current
  IF (TIMELINE_ACTIVE(TL_RMS_TIMER))
    lRMSLastTimeValue = TIMELINE_GET(TL_RMS_TIMER)

  // Timeleft on timeline / 1000 is seconds remaining
  lRMSSeconds = (alTLTimer[1] - lRMSLastTimeValue) / 1000
  RETURN lRMSSeconds;
}

(**********************************************************)
(* Function: RMSTimerGetMinutes                            *)
(* Purpose:  Get Minutes                                  *)
(**********************************************************)
DEFINE_FUNCTION LONG RMSTimerGetMinutes()
{
  RETURN lRMSMinutes;
}

(**********************************************************)
(* Function: RMSTimerGetHours                             *)
(* Purpose:  Get Minutes                                  *)
(**********************************************************)
DEFINE_FUNCTION LONG RMSTimerGetHours()
{
  RETURN lRMSHours;
}

(**********************************************************)
(* Function: RMSTimerGetDays                              *)
(* Purpose:  Get Minutes                                  *)
(**********************************************************)
DEFINE_FUNCTION LONG RMSTimerGetDays()
{
  RETURN lRMSDays;
}

(**********************************************************)
(* Function: RMSTimerGetMonths                            *)
(* Purpose:  Get Minutes                                  *)
(**********************************************************)
DEFINE_FUNCTION LONG RMSTimerGetMonths()
{
  RETURN lRMSMonths;
}

(**********************************************************)
(* Function: RMSTimerGetYears                             *)
(* Purpose:  Get Minutes                                  *)
(**********************************************************)
DEFINE_FUNCTION LONG RMSTimerGetYears()
{
  RETURN lRMSYears;
}

(**************************************)
(* Call Name: RMSTimerTimeDiff        *)
(* Function:  Difference Between Times*)
(* Param:     Time1, Time2, Sec, Min, *)
(*            Hour                    *)
(* Return:    0 = Good                *)
(*           -1 = Time1 Invalid       *)
(*           -2 = Time2 Invalid       *)
(* Notes:    lSec, lMin And lHour     *)
(*           Are Totals               *)
(**************************************)
DEFINE_FUNCTION SINTEGER RMSTimerTimeDiff (CHAR cTime1[], CHAR cTime2[], LONG lSec, LONG lMin, LONG lHour)
STACK_VAR
SLONG slNowMin1
SLONG slNowHour1
SLONG slNowSec1
SLONG slNowMin2
SLONG slNowHour2
SLONG slNowSec2
SLONG slMin
SLONG slHour
SLONG slSec
CHAR  bSwapTimes
{
  // set defaults
  lSec = 0
  lMin = 0
  lHour = 0

  // Break Up Components
  slNowSec1  = TIME_TO_SECOND(cTime1)
  slNowMin1  = TIME_TO_MINUTE(cTime1)
  slNowHour1 = TIME_TO_HOUR(cTime1)
  IF (slNowHour1 < 0 || slNowMin1 < 0 || slNowSec1 < 0)
    RETURN -1;

  // Break Up Components
  slNowSec2  = TIME_TO_SECOND(cTime2)
  slNowMin2  = TIME_TO_MINUTE(cTime2)
  slNowHour2 = TIME_TO_HOUR(cTime2)
  IF (slNowHour2 < 0 || slNowMin2 < 0 || slNowSec2 < 0)
    RETURN -2;

  // Make sure cTime1 > cTime2
  IF (slNowHour1 < slNowHour2)
    slNowHour1 = slNowHour1 + 24

  // Swap dates?
  SELECT
  {
    ACTIVE (slNowHour1 < slNowHour2):
      ON[bSwapTimes]
    ACTIVE (slNowHour1 > slNowHour2):
      OFF[bSwapTimes]
    ACTIVE (slNowMin1 < slNowMin2):
      ON[bSwapTimes]
    ACTIVE (slNowMin1 > slNowMin2):
      OFF[bSwapTimes]
    ACTIVE (slNowSec1 < slNowSec2):
      ON[bSwapTimes]
    ACTIVE (1):
      OFF[bSwapTimes]
  }
  
  // Make sure cTime1 > cTime2
  IF (bSwapTimes)
  {
      slNowSec1  = TIME_TO_SECOND(cTime2)
      slNowMin1  = TIME_TO_MINUTE(cTime2)
      slNowHour1 = TIME_TO_HOUR(cTime2)
    
      slNowSec2  = TIME_TO_SECOND(cTime1)
      slNowMin2  = TIME_TO_MINUTE(cTime1)
      slNowHour2 = TIME_TO_HOUR(cTime1)
  }


  // Now Calc Diff
  // Hour Will Not Be -
  slHour        = slNowHour1 - slNowHour2
  slMin         = slNowMin1  - slNowMin2
  IF (slMin < 0)
  {
    // If This Happens, It Is Because slHour >= 1
    slHour--
    slMin = slMin + 60
  }
  slSec         = slNowSec1  - slNowSec2
  IF (slSec < 0)
  {
    slMin--
    IF (slMin < 0)
    {
      // If This Happens, It Is Because slHour >= 1
      slHour--
      slMin = slMin + 60
    }
    slSec = slSec + 60
  }
  slSec         = slSec   + (slMin * 60)  + (slHour * 60 * 60)
  slMin         = slMin   + (slHour * 60)
  lHour         = ABS_VALUE(TYPE_CAST(slHour))
  lMin          = ABS_VALUE(TYPE_CAST(slMin))
  lSec          = ABS_VALUE(TYPE_CAST(slSec))
  RETURN 0;
}

(**************************************)
(* Call Name: RMSTimerDateDiff        *)
(* Function:  Subtract Dates          *)
(* Param:     Date1, Date2, Day, Mon, *)
(*            Year                    *)
(* Return:    0 = Good                *)
(*           -1 = Date1 Invalid       *)
(*           -2 = Date2 Invalid       *)
(* Notes:    lDay, lMonth And lYear   *)
(*           Are Totals               *)
(**************************************)
DEFINE_FUNCTION SINTEGER RMSTimerDateDiff (CHAR cDate1[], CHAR cDate2[], LONG lDay, LONG lMonth, LONG lYear)
STACK_VAR
SLONG slNowDay1
SLONG slNowMonth1
SLONG slNowYear1
SLONG slNowDay2
SLONG slNowMonth2
SLONG slNowYear2
SLONG slDay
SLONG slMonth
SLONG slYear
CHAR  bSwapDates
{
  // set defaults
  lDay = 0;
  lMonth = 0;
  lYear = 0;
  
  // if dates are the same, then return success and exit function
  IF(cDate1 == cDate2)
    RETURN 0;

  // Break Up Components
  slNowDay1   = DATE_TO_DAY(cDate1)
  slNowMonth1 = DATE_TO_MONTH(cDate1)
  slNowYear1  = DATE_TO_YEAR(cDate1)
  IF (slNowYear1 <= 0 || slNowMonth1 <= 0 || slNowDay1 <= 0)
    RETURN -1;

  // Break Up Components
  slNowDay2   = DATE_TO_DAY(cDate2)
  slNowMonth2 = DATE_TO_MONTH(cDate2)
  slNowYear2  = DATE_TO_YEAR(cDate2)
  IF (slNowYear2 <= 0 || slNowMonth2 <= 0 || slNowDay2 <= 0)
    RETURN -2;
    
  // Swap dates?
  SELECT
  {
    ACTIVE (slNowYear1 < slNowYear2):
      ON[bSwapDates]
    ACTIVE (slNowYear1 > slNowYear2):
      OFF[bSwapDates]
    ACTIVE (slNowMonth1 < slNowMonth2):
      ON[bSwapDates]
    ACTIVE (slNowMonth1 > slNowMonth2):
      OFF[bSwapDates]
    ACTIVE (slNowDay1 < slNowDay2):
      ON[bSwapDates]
    ACTIVE (1):
      OFF[bSwapDates]
  }
  IF (bSwapDates)
  {
    slNowDay1   = DATE_TO_DAY(cDate2)
    slNowMonth1 = DATE_TO_MONTH(cDate2)
    slNowYear1  = DATE_TO_YEAR(cDate2)  
    
    slNowDay2   = DATE_TO_DAY(cDate1)
    slNowMonth2 = DATE_TO_MONTH(cDate1)
    slNowYear2  = DATE_TO_YEAR(cDate1)    
  }

  // Now Calc Diff
  // Year Will Not Be -
  slYear         = slNowYear1   - slNowYear2
  slMonth        = slNowMonth1  - slNowMonth2
  IF (slMonth < 0)
  {
    // If This Happens, It Is Because slYear >= 1
    slYear--
    slMonth = slMonth + 12
  }
  slDay          = slNowDay1    - slNowDay2
  slMonth        = slMonth       + (slYear * 12)
  IF (slDay < 0)   slMonth--
  IF (slMonth < 0) slYear--

  // Wow, Days Is A Bit Harder Than Those... Why Is There A Variable Number Of Days Per Month?
  // Negative slDay Get Fixed Here Since We Include Days Per Month Of This Month...
  WHILE (slNowYear2 < slNowYear1 || slNowMonth2 < slNowMonth1)
  {
    slDay = slDay + RMSTimerDaysPerMonth(slNowMonth2,slNowYear2)
    slNowMonth2++
    IF (slNowMonth2 > 12)
    {
      slNowMonth2 = 1
      slNowYear2++
    }
  }
  // Copy
  lDay   = TYPE_CAST(slDay)
  lMonth = TYPE_CAST(slMonth)
  lYear  = TYPE_CAST(slYear)
  RETURN 0;
}



(**************************************)
(* Call Name: RMSIsLeapYear           *)
(* Function:  Return 1 if Leap Year   *)
(* Param:     Year                    *)
(* Return:    Days Per Month          *)
(* Note:      Deterine if the         *)
(*            provided year is a      *)
(*            leap year or not.       *)
(**************************************)
DEFINE_FUNCTION CHAR RMSIsLeapYear(SLONG slYear)
STACK_VAR
SLONG slTempYear
{
    IF((slYear % 4) != 0)
	RETURN 0;
    IF((slYear % 100) == 0)
	RETURN ((slYear % 400) == 0);
    RETURN 1;
}
	
(**************************************)
(* Call Name: RMSTimerDaysPerMonth    *)
(* Function:  Return Just 'dat        *)
(* Param:     Month, Year             *)
(* Return:    Days Per Month          *)
(* Note:      Super Hack! Allows      *)
(*            Month To Be Outisde     *)
(*            Normal Range And New    *)
(*            Year Is Calc'ed         *)
(**************************************)
DEFINE_FUNCTION CHAR RMSTimerDaysPerMonth (SLONG slMonth, SLONG slYear)
STACK_VAR
SLONG slTempYear
SLONG slTempMonth
{
  slTempMonth = slMonth
  slTempYear = slYear

  // Special Month Adjuster
  WHILE (slTempMonth > 12)
  {
    slTempMonth = slTempMonth - 12
    slTempYear++
  }
  WHILE (slTempMonth <= 0)
  {
    slTempMonth = slTempMonth + 12
    slTempYear--
  }

  // Do It
  SELECT
  {
    ACTIVE (slTempMonth = 2 && RMSIsLeapYear(slTempYear) = 0):  RETURN 29; // Leap Year
    ACTIVE (slTempMonth = 2):                                   RETURN 28;
    ACTIVE (slTempMonth = 4):                                   RETURN 30;
    ACTIVE (slTempMonth = 6):                                   RETURN 30;
    ACTIVE (slTempMonth = 9):                                   RETURN 30;
    ACTIVE (slTempMonth = 11):                                  RETURN 30;
    ACTIVE (1):                                                 RETURN 31;
  }
  RETURN 0;
}

(**************************************)
(* Call Name: RMSTimerTimeAdd         *)
(* Function:  Adjust Times            *)
(* Param:     Time, Sec, Min, Hour,   *)
(*            Day Offset              *)
(* Return:    New Time                *)
(**************************************)
DEFINE_FUNCTION CHAR[10] RMSTimerTimeAdd (CHAR cTime[], SLONG slSec, SLONG slMin, SLONG slHour, SLONG slDayOffset)
STACK_VAR
CHAR cTempTime[10]
SLONG slNowMin
SLONG slNowHour
SLONG slNowSec
{
  // Get Time
  slDayOffset = 0
  cTempTime = cTime
  IF (LENGTH_STRING(cTempTime) == 0)
    cTempTime = TIME
  IF (LENGTH_STRING(cTempTime) <= 5)
    cTempTime = "cTempTime,':00'"

  // Break Up Components
  slNowSec  = TIME_TO_SECOND(cTempTime)
  slNowMin  = TIME_TO_MINUTE(cTempTime)
  slNowHour = TIME_TO_HOUR(cTempTime)
  IF (slNowHour < 0 || slNowMin < 0 || slNowSec < 0)
    RETURN "";

  // Anything To Do?
  IF (slSec == 0 && slMin == 0 && slHour == 0)
    RETURN cTempTime;

  // Now, Adjust Sec
  slDayOffset = 0
  slNowSec = slNowSec + slSec
  WHILE (slNowSec >= 60)
  {
    slNowSec = slNowSec - 60
    slNowMin++
  }
  WHILE (slNowSec < 0)
  {
    slNowSec = slNowSec + 60
    slNowMin--
  }

  // Now, Adjust Min
  slNowMin = slNowMin + slMin
  WHILE (slNowMin >= 60)
  {
    slNowMin = slNowMin - 60
    slNowHour++
  }
  WHILE (slNowMin < 0)
  {
    slNowMin = slNowMin + 60
    slNowHour--
  }

  // Now, Adjust Hour
  slNowHour = slNowHour + slHour
  WHILE (slNowHour >= 24)
  {
    slNowHour = slNowHour - 24
    slDayOffset++
  }
  WHILE (slNowHour < 0)
  {
    slNowHour = slNowHour + 24
    slDayOffset--
  }
  // Now, Put It Back Together
  cTempTime = "FORMAT('%02d',slNowHour),':',FORMAT('%02d',slNowMin),':',FORMAT('%02d',slNowSec)"
  IF (LENGTH_STRING(cTime))
    SET_LENGTH_STRING(cTempTime,LENGTH_STRING(cTime))
  RETURN cTempTime;
}

(**************************************)
(* Call Name: RMSTimerCallbackProxy   *)
(* Function:  My Version of Callback  *)
(* Param:     None                    *)
(* Return:    None                    *)
(* Note:      Caller must define this *)
(**************************************)
DEFINE_FUNCTION RMSTimerCallbackProxy()
{
  // Application Function - Caller Must Implement This Function...
  IF (bRMSDebug)
    SEND_STRING 0,"'RMS Timer: RMSTimerCallbackProxy()'"
  RMSTimerCallback()
  RETURN;
}

(**********************************************************)
(* Function: RMSTimerPrintVersion                          *)
(* Purpose:  Print version                                *)
(**********************************************************)
DEFINE_FUNCTION RMSTimerPrintVersion()
{
  SEND_STRING 0,"'  Running ',__RMS_TIMER_NAME__,', v',__RMS_TIMER_VERSION__"
}

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

// What Version?
RMSTimerPrintVersion()

(***********************************************************)
(*                THE EVENTS GOES BELOW                    *)
(***********************************************************)
DEFINE_EVENT

(*******************************************)
(* TIMLINE: Check for New Appointment      *)
(*******************************************)
TIMELINE_EVENT[TL_RMS_TIMER]
{
  LOCAL_VAR SINTEGER snReturn
  LOCAL_VAR INTEGER nSecNow
  LOCAL_VAR INTEGER nSecToWait
  LOCAL_VAR CHAR    cTime[5]
  LOCAL_VAR CHAR    cPrevTime[5]
  LOCAL_VAR LONG    lSecs
  LOCAL_VAR LONG    lDays
  LOCAL_VAR LONG    lMonths
  LOCAL_VAR LONG    lYears
  LOCAL_VAR LONG    lMinutes
  LOCAL_VAR LONG    lHours
  LOCAL_VAR CHAR    bFireCB

  // Time Change?
  cTime = LEFT_STRING(TIME,5)
  IF (cPrevTime <> cTime)
  {  
    // get days, months and years
    snReturn = RMSTimerDateDiff(LDATE, cRMSRefDate, lDays, lMonths, lYears)

    // if the date diff function returned an error, we need to re-baseline the date
    IF(snReturn != 0)    
    {    
	// Debug 
	IF(bRMSDebug)
	{
	  SEND_STRING 0,"'------------------------------------------------------------------'"
	  SEND_STRING 0,"'RMS Timer: [TL_RMS_TIMER] ERROR Returned From: RMSTimerDateDiff   '"
	  SEND_STRING 0,"'------------------------------------------------------------------'"
	  SEND_STRING 0,"'     Error Number = ',ITOA(snReturn)"
	  SEND_STRING 0,"'     LDATE        = ',LDATE"
	  SEND_STRING 0,"'     cRMSRefDate  = ',cRMSRefDate"
	  SEND_STRING 0,"'     lDays        = ',ITOA(lDays)"
	  SEND_STRING 0,"'     lMonths      = ',ITOA(lMonths)"
	  SEND_STRING 0,"'     lYears       = ',ITOA(lYears)"
	  SEND_STRING 0,"'     cPrevTime    = ',cPrevTime"
	  SEND_STRING 0,"'     cTime        = ',cTime"
	  SEND_STRING 0,"'------------------------------------------------------------------'"
	}
	
	// defaults
	lDays = 0
	lMonths = 0
	lYears = 0
    }
    
    // continue if no error was encountered
    IF(snReturn == 0)
    {
	// get minutes and hours
	snReturn = RMSTimerTimeDiff(TIME, cRMSRefTime, lSecs, lMinutes, lHours)
	
	// if the time diff function returned an error, we need to re-baseline the time
	IF(snReturn != 0)    
	{    
	    // Debug 
	    IF(bRMSDebug)
	    {
		SEND_STRING 0,"'------------------------------------------------------------------'"
		SEND_STRING 0,"'RMS Timer: [TL_RMS_TIMER] ERROR Returned From: RMSTimerTimeDiff   '"
		SEND_STRING 0,"'------------------------------------------------------------------'"
		SEND_STRING 0,"'     Error Number = ',ITOA(snReturn)"
		SEND_STRING 0,"'     TIME         = ',TIME"
		SEND_STRING 0,"'     cRMSRefTime  = ',cRMSRefTime"
		SEND_STRING 0,"'     lSecs        = ',ITOA(lSecs)"
		SEND_STRING 0,"'     lMinutes     = ',ITOA(lMinutes)"
		SEND_STRING 0,"'     lHours       = ',ITOA(lHours)"
		SEND_STRING 0,"'     cPrevTime    = ',cPrevTime"
		SEND_STRING 0,"'     cTime        = ',cTime"
		SEND_STRING 0,"'------------------------------------------------------------------'"
	    }
	    
	    // defaults
	    lSecs = 0
	    lMinutes = 0
	    lHours = 0
	}
    }

    // Debug
    IF (bRMSDebug)
      SEND_STRING 0,"'RMS Timer: Update to ',FORMAT('%02d/',lMonths),FORMAT('%02d/',lDays),FORMAT('%04d ',lYears),FORMAT('%02d:',lHours),FORMAT('%02d:',lMinutes),FORMAT('%02d',lSecs)"

    // Callback?
    OFF[bFireCB]
    SWITCH (nRMSCallbackTime)
    {
      CASE RMS_TIMER_CB_MINUTE:
        IF (lRMSMinutes <> lMinutes) ON[bFireCB]
      CASE RMS_TIMER_CB_HOUR:
        IF (lRMSHours <> lHours)     ON[bFireCB]
      CASE RMS_TIMER_CB_DAY:
        IF (lRMSDays <> lDays)       ON[bFireCB]
      CASE RMS_TIMER_CB_MONTH:
        IF (lRMSMonths <> lMonths)   ON[bFireCB]
      CASE RMS_TIMER_CB_YEAR:
        IF (lRMSYears <> lYears)     ON[bFireCB]
    }

    // Store
    lRMSDays     = lDays
    lRMSMonths   = lMonths
    lRMSYears    = lYears
    lRMSMinutes  = lMinutes
    lRMSHours    = lHours

    // Fire Callback?
    IF (bFireCB)
      RMSTimerCallbackProxy()

    // History
    cPrevTime = cTime
  }

  // Timeline Runs every 60 seconds
  // We need the timeline to hit very close before time changes
  // We will shoot for 100ms before second change
  nSecNow = TYPE_CAST(TIME_TO_SECOND(TIME))

  // If nSecNow == 0, I have done a good job so there is nothing to do!
  // If not, I need to adjust the timeline
  IF (nSecNow > 0)
  {
    // Now, I have nSecToWait to wait.  If this is not 1, I will set for it
   nSecToWait = 60 - nSecNow
    IF (nSecToWait > 1)
    {
      // Well, I will undershoot the time about so we tweak the time down to 100ms after sec change...
      TIMELINE_SET(TL_RMS_TIMER,(nSecNow+1)*1000)
      IF (bRMSDebug)
        SEND_STRING 0,"'RMS Timer: adjusting for ',ITOA(nSecToWait),' sec to wait.'"
    }
    // I am in the fractions of seconds here...
    // The plan is to let the timeline fire every 100 ms until I get it right.
    ELSE
    {
      TIMELINE_SET(TL_RMS_TIMER,alTLTimer[MAX_LENGTH_ARRAY(alTLTimer)]-100)
      IF (bRMSDebug)
        SEND_STRING 0,"'RMS Timer: tweaking for fractions of seconds...'"
    }
  }
}


#END_IF // __RMS_TIMER__
(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT alY CODE BELOW THIS COMMENT           *)
(***********************************************************)