MODULE_NAME='DVX-3150 v1_00'(DEV VIRTUAL)
(***********************************************************)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 04/04/2006  AT: 11:33:16        *)
(***********************************************************)
(* System Type : NetLinx                                   *)
(***********************************************************)
(* REV HISTORY:                                            *)
(***********************************************************)
(*
    $History: $
*)
(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

SWITCHER					=		5002:1:0

AVINPUT_1					=		5002:1:0
AVINPUT_2					=		5002:2:0
AVINPUT_3					=		5002:3:0
AVINPUT_4					=		5002:4:0
AVINPUT_5					=		5002:5:0
AVINPUT_6					=		5002:6:0
AVINPUT_7					=		5002:7:0
AVINPUT_8					=		5002:8:0
AVINPUT_9					=		5002:9:0
AVINPUT_10				=		5002:10:0
AINPUT_11					=		5002:11:0
AINPUT_12					=		5002:12:0
AINPUT_13					=		5002:13:0
AINPUT_14					=		5002:14:0

MIC_1							=		5002:1:0
MIC_2							=		5002:2:0

AOUTPUT_1					=		5002:1:0
AOUTPUT_2					=		5002:2:0
AOUTPUT_3					=		5002:3:0
AOUTPUT_4					=		5002:4:0

AVOUTPUT_1				=		5002:1:0
AVOUTPUT_2				=		5002:2:0
AVOUTPUT_3				=		5002:3:0
AVOUTPUT_4				=		5002:4:0

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT


MAX_VIDEO_INPUTS				=		10
MAX_INPUTS							=		14
MAX_OUTPUTS							=		4
MAX_MICS								=		2

MAX_MIC_GAIN						=		100
MIN_MIC_GAIN						=		0

MAX_INPUT_GAIN					=		24
MIN_INPUT_GAIN					=		0




(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE
structure _DVX
{
	integer nPicMute
	integer nPicFreeze
	char	  cInputNames[14][63]
	integer nInputGain[14]
	integer nMicGain[2]
	integer nMaxOutputVol[4]
	integer nMinOutputVol[4]
	integer nVolume[4]
	integer nMute[4]
	char		cModuleVersion[32]
}
(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

volatile _DVX uDVX

volatile dev	dvAVInputs[]	=	{AVINPUT_1,AVINPUT_2,AVINPUT_3,AVINPUT_4,AVINPUT_5,AVINPUT_6,AVINPUT_7,AVINPUT_8,AVINPUT_9,AVINPUT_10,AINPUT_11,AINPUT_12,AINPUT_13,AINPUT_14}
volatile dev  dvAOutputs[]	=	{AOUTPUT_1,AOUTPUT_2,AOUTPUT_3,AOUTPUT_4}
volatile dev  dvAVOutputs[]	=	{AVOUTPUT_1,AVOUTPUT_2,AVOUTPUT_3,AVOUTPUT_4}
volatile dev  dvMics[]			=	{MIC_1,MIC_2}

volatile char refOnOff[2][32]		=	{{'OFF'},{'ON'}}
volatile char refEnaDis[2][32] = {{'DISABLE'},{'ENABLE'}}
volatile char refTones[11][32]	=	{{'OFF'},{'60HZ'},{'250HZ'},{'400HZ'},{'1KHZ'},{'3KHZ'},{'5KHZ'},{'10KHZ'},{'PINK NOISE'},{'WHITE NOISE'}}
volatile char refPatterns[11][32]	=	{{'OFF'},{'COLOR BAR'},{'GRAY RAMP'},{'SMPTE BAR'},{'HILO TRACK'},{'PLUGE'},{'X-HATCH'},{'LOGO 1'},{'LOGO 2'},{'LOGO 3'}}

(***********************************************************)
(*               LATCHING DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_LATCHING

(***********************************************************)
(*       MUTUALLY EXCLUSIVE DEFINITIONS GO BELOW           *)
(***********************************************************)
DEFINE_MUTUALLY_EXCLUSIVE

(***********************************************************)
(*        SUBROUTINE/FUNCTION DEFINITIONS GO BELOW         *)
(***********************************************************)
(* EXAMPLE: DEFINE_FUNCTION <RETURN_TYPE> <NAME> (<PARAMETERS>) *)
(* EXAMPLE: DEFINE_CALL '<NAME>' (<PARAMETERS>) *)
DEFINE_FUNCTION fnCommand(char Cmd[100])
{
	stack_var char Params[5][32]
	stack_var integer x
	stack_var integer xstart
	stack_var integer Valid
	Cmd = upper_string(Cmd)
	if(find_string(Cmd,'=',1))
	{
		Params[1] = remove_string(Cmd,'=',1)
		Params[1] = left_string(Params[1],length_string(Params[1])-1)
		xstart = 2
	}
	else if(find_string(Cmd,'?',1))
	{
		Params[1] = remove_string(Cmd,'?',1)
		Params[1] = left_string(Params[1],length_string(Params[1])-1)
		xstart = 2
	}
	else
	{
		xstart = 1
	}
	for(x=xstart;x<=5;x++)
	{
		if(find_string(Cmd,':',1))
		{
			Params[x] = remove_string(Cmd,':',1)
			Params[x] = left_string(Params[x],length_string(Params[x])-1)
		}
		else
		{
			Params[x] = Cmd
			BREAK;
		}
	}
	switch(Params[1])
	{
		case 'MIC':
		{
			if(atoi(Params[2]) > MAX_MICS)
			{
				send_string VIRTUAL, "'ERROR:INVALID MIC, VALID MAX = ',itoa(MAX_MICS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvMics[atoi(Params[2])], "'?AUDMIC_ON'"
				}
				else
				{
					send_command dvMics[atoi(Params[2])], "'AUDMIC_ON-',lower_string(Params[3])"
				}
			}
		}
		case 'MIC_POWER':
		{
			if(atoi(Params[2]) > MAX_MICS)
			{
				send_string VIRTUAL, "'ERROR:INVALID MIC, VALID MAX = ',itoa(MAX_MICS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvMics[atoi(Params[2])], "'?AUDMIC_PHANTOM_PWR'"
				}
				else
				{
					send_command dvMics[atoi(Params[2])], "'AUDMIC_PHANTOM_PWR-',lower_string(Params[3])"
				}
			}
		}
		case 'MIC_GAIN':
		{
			if(atoi(Params[2]) > MAX_MICS)
			{
				send_string VIRTUAL, "'ERROR:INVALID MIC, VALID MAX = ',itoa(MAX_MICS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvMics[atoi(Params[2])], "'?AUDMIC_PREAMP_GAIN'"
				}
				else
				{
					if(atoi(Params[3]) <= MAX_MIC_GAIN && atoi(Params[3]) >= MIN_MIC_GAIN)
					{
						send_command dvMics[atoi(Params[2])], "'AUDMIC_PREAMP_GAIN-',lower_string(Params[3])"
						uDVX.nMicGain[atoi(Params[2])] = atoi(Params[3])
					}
					else
					{
						send_string VIRTUAL, "'ERROR:INVAID GAIN, VALID RANGE ',itoa(MAX_MIC_GAIN),' to ',itoa(MIN_MIC_GAIN)"
					}
				}
			}
		}
		case 'MAX_OUT_VOL':
		{
			if(atoi(Params[2]) > MAX_OUTPUTS)
			{
				send_string VIRTUAL, "'ERROR:INVALID OUTPUT, VALID MAX = ',itoa(MAX_OUTPUTS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvAOutputs[atoi(Params[2])], "'?AUDOUT_MAXVOL'"
				}
				else
				{
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_MAXVOL-',lower_string(Params[3])"
					uDVX.nMaxOutputVol[atoi(Params[2])] = atoi(Params[3])
				}
			}
		}
		case 'MIN_OUT_VOL':
		{
			if(atoi(Params[2]) > MAX_OUTPUTS)
			{
				send_string VIRTUAL, "'ERROR:INVALID OUTPUT, VALID MAX = ',itoa(MAX_OUTPUTS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvAOutputs[atoi(Params[2])], "'?AUDOUT_MINVOL'"
				}
				else
				{
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_MINVOL-',lower_string(Params[3])"
					uDVX.nMinOutputVol[atoi(Params[2])] = atoi(Params[3])
				}
			}
		}
		case 'INPUT_GAIN':
		{
			if(atoi(Params[2]) > MAX_MICS)
			{
				send_string VIRTUAL, "'ERROR:INVALID INPUT, VALID MAX = ',itoa(MAX_INPUTS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvMics[atoi(Params[2])], "'?AUDIN_GAIN'"
				}
				else
				{
					if(atoi(Params[3]) <= MAX_INPUT_GAIN && atoi(Params[3]) >= MIN_INPUT_GAIN)
					{
						send_command dvAVInputs[atoi(Params[2])], "'AUDIN_GAIN-',lower_string(Params[3])"
						uDVX.nInputGain[atoi(Params[2])] = atoi(Params[3])
					}
					else
					{
						send_string VIRTUAL, "'ERROR:INVAID GAIN, VALID RANGE ',itoa(MIN_INPUT_GAIN-24),' to ',itoa(MAX_INPUT_GAIN)"
					}
				}
			}
		}
		case 'TEST_TONE':
		{
			for(x=1;x<=11;x++)
			{
				if(Params[3] = refTones[x] || Params[3] = '?')
				{
					Valid = 1;
					BREAK;
				}
				if(x=11)
				{
					send_string VIRTUAL, "'ERROR: INVAILD TEST TONE'"
				}
			}
			if(Valid)
			{
				if(Params[3] = '?')
				{
					send_command dvAOutputs[atoi(Params[2])], "'?AUDOUT_TESTTONE'"
				}
				else
				{
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_TESTTONE=',Params[3]"
				}
			}
		}
		case 'INPUT_NAME':
		{
			if(atoi(Params[2]) > MAX_INPUTS)
			{
				send_string VIRTUAL, "'ERROR: INVALID INPUT, MAX INPUT = ',itoa(MAX_INPUTS)"
			}
			else
			{
				if(Params[3] = '?')
				{
					send_command dvAVInputs[atoi(Params[2])], "'?VIDIN_NAME'"
				}
				else
				{
					send_command dvAVInputs[atoi(Params[2])], "'VIDIN_NAME-',Params[3]"
					uDVX.cInputNames[atoi(Params[2])] = Params[3]
				}
			}
		}
		case 'PIC_MUTE':
		{
			if(Params[2] = '?')
			{
				send_command dvAVOutputs[atoi(Params[2])], "'?VIDOUT_MUTE'"
			}
			else
			{
				for(x=1;x<=2;x++)
				{
					if(Params[3] = refOnOff[x])
					{
						Valid  = x
						BREAK;
					}
				}
				if(Params[3] = '1'||Params[3] = '0')
				{
					send_command dvAVOutputs[atoi(Params[2])], "'VIDOUT_MUTE-',refEnaDis[atoi(Params[3])+1]"
				}
				else
				{
					send_command dvAVOutputs[atoi(Params[2])], "'VIDOUT_MUTE-',refEnaDis[Valid]"
				}
				uDVX.nPicMute = Valid
			}
		}
		case 'PIC_FREEZE':
		{
			if(Params[2] = '?')
			{
				send_command SWITCHER, "'?VIDOUT_FREEZE'"
			}
			else
			{
				for(x=1;x<=2;x++)
				{
					if(Params[2] = refOnOff[x])
					{
						Valid  = x
						BREAK;
					}
				}
				send_command SWITCHER, "'VIDOUT_FREEEZE-',refEnaDis[Valid]"
				uDVX.nPicFreeze = Valid-1
			}
		}
		case 'TEST_PATTERN':
		{
			for(x=1;x<=11;x++)
			{
				if(Params[3] = refPatterns[x] || Params[3] = '?')
				{
					Valid = 1;
					BREAK;
				}
				if(x=11)
				{
					send_string VIRTUAL, "'ERROR: INVAILD TEST PATTERN'"
				}
			}
			if(Valid)
			{
				if(Params[3] = '?')
				{
					send_command dvAVOutputs[atoi(Params[2])], "'?VIDOUT_TESTPAT'"
				}
				else
				{
					send_command dvAVOutputs[atoi(Params[2])], "'VIDOUT_TESTPAT-',Params[3]"
				}
			}
		}
		case 'VOLUME':
		{
			if(Params[3] = 'MUTE_ON' || Params[3] = 'MUTE_OFF')
			{
				if(Params[3] = 'MUTE_ON')
				{
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_VOLUME-0'"
					uDVX.nMute[atoi(Params[2])] = 1
				}
				else
				{
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_VOLUME-',itoa(uDVX.nVolume[atoi(Params[2])])"
					uDVX.nMute[atoi(Params[2])] = 0
				}
			}
			else
			{
				if(Params[3] = 'UP' || Params[3] = 'DOWN')
				{
					switch(Params[3])
					{
						case 'UP':
						{
							uDVX.nVolume[atoi(Params[2])] ++
						}
						case 'DOWN':
						{
							uDVX.nVolume[atoi(Params[2])] --
						}
					}
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_VOLUME-',itoa(uDVX.nVolume[atoi(Params[2])])"
				}
				else
				{
					send_command dvAOutputs[atoi(Params[2])], "'AUDOUT_VOLUME-',Params[3]"
					uDVX.nVolume[atoi(Params[2])] = atoi(Params[3])
				}
			}
		}
		case 'SWITCH':
		{
			if(Params[2] = 'BOTH')
			{
				Params[2] = 'ALL'
			}
			send_command SWITCHER, "'CL',Params[2],'I',Params[3],'O',Params[4]"
		}
		case 'HDCP':
		{
			send_command dvAVInputs[atoi(Params[2])], "'VIDIN_HDCP-',Params[3]"
		}
		case 'INPUT_TYPE':
		{
			send_command dvAVInputs[atoi(Params[2])], "'VIDIN_FORMAT-',Params[3]"
		}
		case 'RESOLUTION':
		{
			send_command dvAVOutputs[atoi(Params[2])], "'VIDOUT_RES_REF-',Params[3]"
		}
		case 'SCALING':
		{
			send_command dvAVOutputs[atoi(Params[2])], "'VIDOUT_SCALE-',Params[3]"
		}
		case 'VIDEO_OUT':
		{
			send_command dvAVOutputs[atoi(Params[2])], "'VIDOUT_ON-',Params[3]"
		}
		case 'VERSION':
		{
			send_string VIRTUAL, "'VERSION=',uDVX.cModuleVersion"
		}
	}
}

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START
uDVX.cModuleVersion = 'v1.00'
(***********************************************************)
(*                THE EVENTS GO BELOW                      *)
(***********************************************************)
DEFINE_EVENT
DATA_EVENT[Virtual]
{
	COMMAND:
	{
		fnCommand(data.text)
	}
}
(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)
