PROGRAM_NAME='LIGHTS.AXI'

DEFINE_DEVICE
//LIGHT    =	5001:7:0
(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT



(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE
// DYNALITE MODULE VARIABLES
CHAR AREA_[] = '11'		// DYNALITE AREA 0 IS ALL AREAS
char JOIN_BYTE = $FF		// JOIN BYTE USED BY DYNALITE TO JOIN ROOMS, $FF IS ALL JOINS IN AN AREA
char PresetInArea[255]		// FEEDBACK OF PRESETS IN AREAS

// CBUS MODULE VARIABLES
char    IPaddr[15] 	= '0.0.0.0' // 0.0.0.0 IF IP NOT USED
integer IPport     	= 14000     // IP port USUALLY 14000
CHAR	LIGHT_APP[] 	= '38'	    // APPLICATION OF LIGHTS USUALLY '38'
CHAR	NETWORK[] 	= '00'	    // NETWORK OF LIGHTS USUALLY '00'

INTEGER BD_LIGHT_STATE
(***********************************************************)
(*               LATCHING DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_LATCHING

(***********************************************************)
(*       MUTUALLY EXCLUSIVE DEFINITIONS GO BELOW           *)
(***********************************************************)
DEFINE_MUTUALLY_EXCLUSIVE


(***********************************************************)
(*           SUBROUTINE DEFINITIONS GO BELOW               *)
(***********************************************************)
DEFINE_MODULE 'DynaLiteMod'    		DLmod(Light, vdvLight, JOIN_BYTE, PresetInArea)


(************************** LIGHT CALLS  ******************************) 
DEFINE_CALL 'LIGHTS'(LEV_) 
{
LOCAL_VAR CHAR LIGHT_LEV[3]
LIGHT_LEV = ITOA(LEV_)
 // DYNALITE COMMANDS
 //			  L:[AREA:CHANNEL]:LEVEL:FADE TIME
 //Send_command vLight, "'L:[',AREA_,':2]:',LIGHT_LEV,':2'" // FLUROS
 //Send_command vLight, "'L:[',AREA_,':2]:',LIGHT_LEV,':2'" // FLUROS
 //Send_command vLight, "'L:[',AREA_,':2]:',LIGHT_LEV,':2'" // FLUROS
 
 // CBUS COMMANDS
 //			K:P:[NETWORK:APPLICATION]:LEVEL:FADE:TIME
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':22]:',LIGHT_LEV,':2'" // FLUROS
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':1C]:',LIGHT_LEV,':0'" // DOWN LIGHTS x 5
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':1D]:',LIGHT_LEV,':0'" // PENDENT x 6 
 IF(LEV_ = 100)
  {
  Send_command vdvLight, "'K:P:[',AREA_,']:1:2'" // FLUROS
  }
IF(LEV_ = 75)
  {
  Send_command vdvLight, "'K:P:[',AREA_,']:2:2'"
  }
IF(LEV_ = 25)
  {
  Send_command vdvLight, "'K:P:[',AREA_,']:3:2'"  
  }
IF(LEV_ = 0)
  {
  Send_command vdvLight, "'K:P:[',AREA_,']:4:2'"
  Send_command vdvLight, "'L:[',AREA_,':1]:0:2'" // FRONT ROW

  }
}

DEFINE_CALL 'LIGHTS_VIDEO'
 {
  Send_command vdvLight, "'K:P:[',AREA_,']:5:2'"
  WAIT 1
  Send_command vdvLight, "'L:[',AREA_,':1]:0:2'" // FRONT ROW
  BD_LIGHT_STATE = 0
  //OFF[RELAYS,LIGHTS_FRONT]
  //ON[RELAYS,LIGHTS_REAR] 
  //OFF[RELAYS,LIGHTS_BOARD]

 // DYNALITE COMMANDS
 //			  L:[AREA:CHANNEL]:LEVEL:FADE TIME
 //Send_command vLight, "'L:[',AREA_,':2]:50:2'" // FLUROS
 //Send_command vLight, "'L:[',AREA_,':2]:80:2'" // FLUROS
 //Send_command vLight, "'L:[',AREA_,':2]:80:2'" // FLUROS

 // CBUS COMMANDS
 //     		K:P:[NETWORK:APPLICATION:GROUP ADDRESS]:LEVEL:FADE:TIME
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':22]:70:2'" // FLUROS
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':1C]:0:2'"  // DOWN LIGHTS X 4
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':1D]:0:2'"  // DOWN LIGHTS X 6
 }

DEFINE_CALL 'LIGHTS_VC'
 {
  Send_command vdvLight, "'K:P:[',AREA_,']:2:2'"
  BD_LIGHT_STATE = 5
  //OFF[RELAYS,LIGHTS_FRONT]
  //OFF[RELAYS,LIGHTS_REAR] 
  //ON[RELAYS,LIGHTS_BOARD]

 // DYNALITE COMMANDS
 //			  L:[AREA:CHANNEL]:LEVEL:FADE TIME
 //Send_command vLight, "'L:[',AREA_,':2]:100:2'" // FLUROS
 //Send_command vLight, "'L:[',AREA_,':2]:100:2'" // FLUROS
 //Send_command vLight, "'L:[',AREA_,':2]:90:2'" // FLUROS
 
 // CBUS COMMANDS
 //     		K:P:[NETWORK:APPLICATION:GROUP ADDRESS]:LEVEL:FADE:TIME
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':22]:100:2'" // FLUROS
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':1C]:40:2'"  // DOWN LIGHTS X 5
 //Send_command vLight, "'K:P:[',NETWORK,':',LIGHT_APP,':1D]:40:2'"  // DOWN LIGHTS X 6
 }


(***********************************************************)
(*                   EVENTS GO HERE YAY                    *)
(***********************************************************)
DEFINE_EVENT

// LIGHTS ALL ON
BUTTON_EVENT[vdvTP,60] {PUSH:{CALL 'LIGHTS'(100)}}

// LIGHTS 75
BUTTON_EVENT[vdvTP,61] {PUSH:{CALL 'LIGHTS'(75)}}

// LIGHTS 25
BUTTON_EVENT[vdvTP,62] {PUSH:{CALL 'LIGHTS'(25)}}

// LIGHTS ALL OFF
BUTTON_EVENT[vdvTP,63] {PUSH:{CALL 'LIGHTS'(0)}}

// LIGHTS VIDEO
BUTTON_EVENT[vdvTP,64] {PUSH:{CALL 'LIGHTS_VIDEO'}}


(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START
