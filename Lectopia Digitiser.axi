PROGRAM_NAME='Lectopia Digitiser'
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 01/25/2010  AT: 23:50:33        *)
(***********************************************************)
#if_not_defined LECTOPIA_DIGITISER_AXI
#define LECTOPIA_DIGITISER_AXI
(***********************************************************)
(* Roger McLean, Swinburne University 20090604             *)
(* Example program for using the Lectopia module v2.0.0.   *)
(* See documentation for in-depth description.             *)
(***********************************************************)

(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

dvLect			 	= 10001:18:0
vdvLect            	= 33018:1:0
vdvCapHardwarePort 	= 0:18:0
vdvLectServerPort  	= 0:19:0

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT
//TP feedback buttons
btSpotLights = 10

btCPOnline    = 21

btCPIdle      = 31
btCPPaused    = 32
btCPTrigger   = 33
btCPRecording = 34
btCPFinishing = 35

btCPSyncAudio = 41
btCPSyncVideo = 42
btCPSyncVGA   = 43

//TP variable text
vtUnitName = 251
vtUnitCode = 252
vtRecType  = 253
vtTime     = 254
vtVenue    = 250
vtDevice   = 192  
vtDigiType = 193  
vtDigiState= 191  

//Channels for Lectopia module
char chAudio        = 231
char chVideo        = 232
char chVGA          = 233

char chIdle         = 241
char chPaused       = 242
char chTrigger      = 243  //Digitiser only.
char chRecording    = 244
char chFinalising   = 245  //CAs only.

char chDeviceOnline = 254

//TouchPanel keypad/keyboard states
kbNone      = 0
kbDigiIP    = 1
kbDigiVenue = 2

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

char levLeft  //See docs re Capture Appliance left channel.
char levRight

char venue[100]
char DeviceIp[100]
char DeviceType[100]

volatile char kbState

(***********************************************************)
(*               LATCHING DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_LATCHING

(***********************************************************)
(*       MUTUALLY EXCLUSIVE DEFINITIONS GO BELOW           *)
(***********************************************************)
DEFINE_MUTUALLY_EXCLUSIVE

(***********************************************************)
(*        SUBROUTINE/FUNCTION DEFINITIONS GO BELOW         *)
(***********************************************************)

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START
create_level vdvLect, 1, levLeft
create_level vdvLect, 2, levRight

(***********************************************************)
(*                MODULES ARE DECLARED BELOW               *)
(***********************************************************)
define_module 'Lectopia' modLectopia(vdvLect, vdvCapHardwarePort, vdvLectServerPort)

(***********************************************************)
(*               FUNCTION DEFINITIONS GO BELOW             *)
(***********************************************************)
define_function SyncTpDigiSettings(){
   send_command vdvLect, 'VENUE?'
   send_command vdvLect, 'DEVICE?'
   send_command vdvLect, 'DEVICE TYPE?'
}

(***********************************************************)
(*                THE EVENTS GO BELOW                      *)
(***********************************************************)
DEFINE_EVENT
level_event[vdvLect,1]{
 send_level dvLect,1,level.value
}
level_event[vdvLect,2]{
 send_level dvLect,2,level.value
}

button_event[dvLect,1]{
 push:{
  to[dvLect,1]
  send_command vdvLect,'STOP'
 }
}

button_event[dvLect,2]{
 push:{
  to[dvLect,2]
  send_command vdvLect,'PAUSE'
 }
}

button_event[dvLect,3]{
 push:{
  to[dvLect,3]
  send_command vdvLect,'RESUME'
 }
}

button_event[dvLect,4]{
 push:{
  to[dvLect,4]
  send_command vdvLect,'DEVICE EXTEND=5'
 }
}

button_event[dvLect,192]{
 push:{
  kbState = kbDigiIP
  send_command dvLect, "'AKEYB-',DeviceIp"
 }
}

button_event[dvLect,250]{
 push:{
  kbState = kbDigiVenue
  send_command dvLect, "'AKEYB-',Venue"
 }
}

button_event[dvLect,193]{
 push:{
  select
   {
   active(DeviceType='digitiser'):{
        DeviceType='captureappliance'
        send_command dvLect, "'!T',vtDigiType,'Digitiser type: ',DeviceType"
        send_command vdvLect,'DEVICE USERNAME=lectopia'  
        send_command vdvLect,'DEVICE PASSWORD=lectopia' 
        send_command vdvLect,'DEVICE TYPE=captureappliance' 
        wait 05 SyncTPDigiSettings()
	}
   active(DeviceType='captureappliance'):{
	DeviceType='digitiser'
	send_command dvLect, "'!T',vtDigiType,'Digitiser type: ',DeviceType"
        send_command vdvLect,'DIGI INTSYSKEY=lectopia'
	send_command vdvLect,'DEVICE TYPE=digitiser' 
        wait 05 SyncTpDigiSettings()
	}
   active(DeviceType=''):{
        wait 05 SyncTpDigiSettings()
	}
    }
 }
}

data_event[vdvLect]{
 online:{
  //Adjust these settings to suit your Lectopia installation
  //send_command vdvLect,'SERVER=lectures.murdoch.edu.au'
  //send_command vdvLect,'SERVER INTSYSKEY=AMX-1DSMn7'
  //send_command vdvLect,'VENUE=Capture test 2'
  //send_command vdvLect,'DEVICE REFRESH=5'  //5 seconds
  //send_command vdvLect,'DIGI COUNTDOWN=2'  //Digitiser only
  //send_command vdvLect,'DIGI INTSYSKEY=lectopia'
  //send_command vdvLect, 'SERVER REFRESH=0'  //Do not poll the server continually. 
  //send_command vdvLect, 'SERVER REFRESH'  //Hit the server once to get the details for the specified venue. 

  //Alternatively you can bypass the server all together by doing the following: 
  send_command vdvLect,"'VENUE=',LECTOPIA_VENUE"
  //send_command vdvLect,'VENUE=Capture test 2' 
  send_command vdvLect,"'DEVICE=',LECTOPIA_IP"   //The IP of the device.  This will be venue-specific. 
  //send_command vdvLect,'DEVICE=10.50.40.71'  //The IP of the device.  This will be venue-specific. 
  //send_command vdvLect,'DEVICE TYPE=digitiser' 
  send_command vdvLect,'DEVICE TYPE=captureappliance' 
  //send_command vdvLect,'SERVER REFRESH=0'  //Never poll the server. 
  send_command vdvLect,'DEVICE USERNAME=lecturer'  //This is per-venue.  The CA picks up it's credentials from the Lectopia server, so it has to match that. 
  send_command vdvLect,'DEVICE PASSWORD=avstore'  //Same deal as per username. 
  send_command vdvLect,'DEVICE REFRESH=5'  //Poll the device every 5 seconds. 
  send_command vdvLect,'DIGI COUNTDOWN=2'  //Digitiser only
  send_command vdvLect,'DIGI INTSYSKEY=lectopia'
  send_command vdvLect,'SERVER INTSYSKEY=AMX-1DSMn7'
 }
 
 string:{
  select{
   active(find_string(data.text,'UNIT NAME=',1)):{
    remove_string(data.text,'UNIT NAME=',1)
    send_command dvLect,"'!T',vtUnitName,data.text"
   }
   active(find_string(data.text,'UNIT CODE=',1)):{
    remove_string(data.text,'UNIT CODE=',1)
    send_command dvLect,"'!T',vtUnitCode,data.text"
   }
   active(find_string(data.text,'VENUE=',1)):{
    remove_string(data.text,'VENUE=',1)
    send_command dvLect,"'!T',vtVenue,data.text"
    venue = data.text;
   }
   active(find_string(data.text,'DEVICE=',1)):{
    remove_string(data.text,'DEVICE=',1)
    send_command dvLect,"'!T',vtDevice,data.text"
    DeviceIp = data.text;
   }
   active(find_string(data.text,'DEVICE TYPE=',1)):{
    remove_string(data.text,'DEVICE TYPE=',1)
    send_command dvLect, "'!T',vtDigiType,'Digitiser type: ',data.text"
    DeviceType = data.text;
   }
   active(find_string(data.text,'RECORDING SETTING=',1)):{
    remove_string(data.text,'RECORDING SETTING=',1)
    send_command dvLect,"'!T',vtRecType,data.text"
    //We can use the AMX to turn on/off spot lights to improve the quality of video recordings.
    //If the capture platform is Echo360 Capture Appliance the 'Video' string will be returned
    //in both Video and Dual Video recordings.  If the capture platform is Digitiser it will
    //depend on the name of the Recording Setting in the Lectopia Server.
    if(find_string(data.text,'Video',1))
     on[dvLect,btSpotLights]
    else
     off[dvLect,btSpotLights]
   }
   active(find_string(data.text,'TIME=',1)):{
    remove_string(data.text,'TIME=',1)
    send_command dvLect,"'!T',vtTime,data.text"
   }
  }
 }
}

data_event[TP]{ //this must equal the main TP variable or it will not work..
 online:{
  wait 60{
    SyncTpDigiSettings()
 }
 }
 string:
	{
	send_string 0, "'STRING RECEIVED FROM PANEL'"
  if(find_string(data.text, 'KEYB-', 1))
	{
   remove_string(data.text, 'KEYB-', 1)
   if(!find_string(data.text, 'ABORT', 1))
		{//That is, 'ABORT' not found.
		send_string 0, "'ABORT STRING NOT FOUND'"
    switch(kbState)
		{
     case kbDigiIP:
			{
      DeviceIp = data.text;
      send_command vdvLect, "'DEVICE=',DeviceIp"
      send_command vdvLect, 'DEVICE?'
//      break;
			}
     case kbDigiVenue:
			{
      venue = data.text;
      send_command vdvLect, "'VENUE=',venue"
      send_command vdvLect, 'VENUE?'
//      break;
			}
    }
    kbState = kbNone;
   }
  }
 }
}

channel_event[vdvLect,chIdle]{
 on:{
  send_command dvLect, "'!T',vtDigiState,'Lecture is: Stopped'"
    }
 off:{ }
}
channel_event[vdvLect,chPaused]{
 on:{
  send_command dvLect, "'!T',vtDigiState,'Lecture is: Paused'"
 }
 off:{ }
}
channel_event[vdvLect,chTrigger]{
 on:{
  send_command dvLect, "'!T',vtDigiState,'Lecture is: Waiting to start'"
 }
 off:{ }
}
channel_event[vdvLect,chRecording]{
 on:{
  send_command dvLect, "'!T',vtDigiState,'Lecture is: Recording'"
 }
 off:{ }
}
channel_event[vdvLect,chFinalising]{
 on:{
  send_command dvLect, "'!T',vtDigiState,'Lecture is: Finalising'"
 }
 off:{ }
}
channel_event[vdvLect,chDeviceOnline]{
 off:{
  send_command dvLect, "'!T',vtDigiState,'Lecture is: Offline'"
 }
 on:{ }
}


(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

[dvLect,btCPOnline]=[vdvLect,chDeviceOnline]

[dvLect,btCPIdle]=[vdvLect,chIdle]
[dvLect,btCPPaused]=[vdvLect,chPaused]
[dvLect,btCPTrigger]=[vdvLect,chTrigger]
[dvLect,btCPRecording]=[vdvLect,chRecording]
[dvLect,200]=[vdvLect,chRecording]
[dvLect,btCPFinishing]=[vdvLect,chFinalising]

[dvLect,btCPSyncAudio]=[vdvLect,chAudio]
[dvLect,btCPSyncVideo]=[vdvLect,chVideo]
[dvLect,btCPSyncVGA]=[vdvLect,chVGA]

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)
#end_if