PROGRAM_NAME='RMSMain_HDX_SYS'


// ENSURE THAT YOU ARE USING DISPLAY MODULE VERSION 35
// IF YOU ARE USING SONY PROJECTORS FOR ERROR REPORT TO WORK

DEFINE_DEVICE

//RMS DEVICES
dvRMSSocket 	= 0:3:0		// MeetingManager IP Socket
vdvRMSEngine 	= 33501:1:0	// MeetingManager Virtual Device
vdvCLActions 	= 33502:1:0	// i!-ConnectLinx Virtual Device

dvSystem	= 0:1:0



(***********************************************************)
(*              CONSTANT DEFINITIONS GO BELOW              *)
(***********************************************************)
DEFINE_CONSTANT

CHAR RMS_SERVER_IP[100] = 'rmsprod01.csumain.csu.edu.au'		// MeetingManager Server IP Address
//CHAR RMS_SERVER_IP[100] = '192.168.1.190'		// MeetingManager Server IP Address


FILTER_MINUTES	= 1
FILTER_HOURS 	= 2
TOTAL_HOURS 	= 3

MOTION_TIMEOUT  = 3000

//PC_STATUS_IO	= 2	// IO FB OF PC STATUS
RMS_MAX_PARAM_LEN         = 100


(***********************************************************)
(*                 INCLUDE FILES GO BELOW                  *)
(***********************************************************)

#INCLUDE 'RMSCommon.axi'

(***********************************************************)
(*              VARIABLE DEFINITIONS GO BELOW              *)
(***********************************************************)
DEFINE_VARIABLE

VOLATILE SLONG   asnNumberLevelArgValues[3]		// i-ConnectLinx paramater storage
VOLATILE CHAR    acStringEnumArgValues[3][50]
VOLATILE INTEGER MOVEMENT
PERSISTENT INTEGER DISPLAY_FILTER_TIME[5][3]	// MINUTES,FILTER_HOURS,TOTAL_RUN_TIME
VOLATILE INTEGER OUT_OF_HOURS
VOLATILE INTEGER PC_POWER
volatile slong acRMSDigitiserOnline
volatile char  acRMSDigitiserState[RMS_MAX_PARAM_LEN]
volatile char  acRMSDigitiserVersion[RMS_MAX_PARAM_LEN]

//devchan vdvLectStates[] = {{vdvLect,241},{vdvLect,242},{vdvLect,243},{vdvLect,244},{vdvLect,245},{vdvLect,249}}
volatile char RMS_LectStealth[3]  //OFF | ONE | ALL

volatile integer nDisplayStringsSent[3]
volatile integer nDSPStringsSent
volatile integer nVCStringsSent

(***********************************************************)
(*             SUBROUTINE DEFINITIONS GO BELOW             *)
(***********************************************************)

DEFINE_FUNCTION RMSDevMonRegisterCallback()
{
	stack_var integer i
// REPLICATE WHAT IS IN THE REGISTRATION SECTION ABOVE WHEN SERVER COMES ONLINE
	//Loop through 3 displays in the system
	for(i = 1; i<= 3; i++){
		//If there is a display then register it
		if(RMS_DISPLAY_TYPE[i] != ''){
			RMSRegisterDevice (DISPLAY[i],RMS_DISPLAY_TYPE[i],RMS_DISPLAY_BRAND[i],RMS_DISPLAY_MODEL[i])		// Register the Projector's Name, Manufacturer and Model
												// when the RMS Engine Module connects to the RMS Server.

			RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
						(DISPLAY[i],			// Declare which device the parameter is for
						'Current Input',		// Name the parameter
						'No Input',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
						RMS_COMP_NONE,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
						RMS_STAT_EQUIPMENT_USAGE,	// Set the notification type - in this case it is a equipment usage notification
						FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
						'No Input',			// Set the reset value (but because we cannot reset, set this value to No Input)
						RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
						'No Input',			// Declare what enumeration the parameter should start on
						'No Input|DVI Input|VGA Input|
						Component Input|S-Video Input|
						Composite Input|HDMI Input')		// Declare the enumeration list


			RMSRegisterDeviceNumberParamWithUnits(DISPLAY[i],			//dvDPS,
						'Filter Time',			//cName,
						'Hours',			//cUnits,
						1000,				//slThreshold,
						RMS_COMP_GREATER_THAN,		//nThresholdCompare,
						RMS_STAT_MAINTENANCE,		//nThresholdStatus,
						true,				//bCanReset,
						0,				//slResetValue,
						RMS_PARAM_SET,			//nInitialOp,
						DISPLAY_FILTER_TIME[i][FILTER_HOURS],			//slInitial,
						0,				//slMin,
						5000)				//slMax)

			RMSRegisterDeviceNumberParamWithUnits(DISPLAY[i],			//dvDPS,
						'Total Run Time',		//cName,
						'Hours',			//cUnits,
						2000,				//slThreshold,
						RMS_COMP_GREATER_THAN,		//nThresholdCompare,
						RMS_STAT_EQUIPMENT_USAGE,	//nThresholdStatus,
						true,				//bCanReset,
						0,				//slResetValue,
						RMS_PARAM_SET,			//nInitialOp,
						DISPLAY_FILTER_TIME[i][TOTAL_HOURS],			//slInitial,
						0,				//slMin,
						30000)				//slMax)

			RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
						(DISPLAY[i],			// Declare which device the parameter is for
						'Display Error',		// Name the parameter
						'No Error',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
						RMS_COMP_NOT_EQUAL_TO,		// Set the conditon that has to be met for an error to occur - in this case there is No threshold
						RMS_STAT_MAINTENANCE,		// Set the notification type - in this case it is a equipment usage notification
						TRUE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
						'No Error',			// Set the reset value (but because we cannot reset, set this value to No Input)
						RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
						'No Error',			// Declare what enumeration the parameter should start on
						'No Error|General Error|Lamp Error|Temperature Error|
						Cover Error|Comms Error')			// Declare the enumeration list
			}
		}
		#IF_DEFINED COLLABORATION_SPACE
			for(i = 1; i<= 6; i++){
			//If there is a display then register it
			if(i <= NUMBER_OF_MIDS){
				RMSRegisterDevice (dvMIDS[i],RMS_MIDS_TYPE,RMS_MIDS_BRAND,RMS_MIDS_MODEL)		// Register the Projector's Name, Manufacturer and Model
													// when the RMS Engine Module connects to the RMS Server.

				RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
							(dvMIDS[i],			// Declare which device the parameter is for
							'Current Input',		// Name the parameter
							'No Input',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
							RMS_COMP_NONE,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
							RMS_STAT_EQUIPMENT_USAGE,	// Set the notification type - in this case it is a equipment usage notification
							FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
							'No Input',			// Set the reset value (but because we cannot reset, set this value to No Input)
							RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
							'No Input',			// Declare what enumeration the parameter should start on
							'No Input|DVI Input|VGA Input|
							Component Input|S-Video Input|
							Composite Input|HDMI Input')		// Declare the enumeration list

				RMSRegisterDeviceNumberParamWithUnits(dvMIDS[i],			//dvDPS,
							'Filter Time',			//cName,
							'Hours',			//cUnits,
							1000,				//slThreshold,
							RMS_COMP_GREATER_THAN,		//nThresholdCompare,
							RMS_STAT_MAINTENANCE,		//nThresholdStatus,
							true,				//bCanReset,
							0,				//slResetValue,
							RMS_PARAM_SET,			//nInitialOp,
							DISPLAY_FILTER_TIME[i][FILTER_HOURS],			//slInitial,
							0,				//slMin,
							5000)				//slMax)

				RMSRegisterDeviceNumberParamWithUnits(dvMIDS[i],			//dvDPS,
							'Total Run Time',		//cName,
							'Hours',			//cUnits,
							2000,				//slThreshold,
							RMS_COMP_GREATER_THAN,		//nThresholdCompare,
							RMS_STAT_EQUIPMENT_USAGE,	//nThresholdStatus,
							true,				//bCanReset,
							0,				//slResetValue,
							RMS_PARAM_SET,			//nInitialOp,
							DISPLAY_FILTER_TIME[i][TOTAL_HOURS],			//slInitial,
							0,				//slMin,
							30000)				//slMax)

				RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
							(dvMIDS[i],			// Declare which device the parameter is for
							'Display Error',		// Name the parameter
							'No Error',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
							RMS_COMP_NOT_EQUAL_TO,		// Set the conditon that has to be met for an error to occur - in this case there is No threshold
							RMS_STAT_MAINTENANCE,		// Set the notification type - in this case it is a equipment usage notification
							TRUE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
							'No Error',			// Set the reset value (but because we cannot reset, set this value to No Input)
							RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
							'No Error',			// Declare what enumeration the parameter should start on
							'No Error|General Error|Lamp Error|Temperature Error|
							Cover Error|Comms Error')			// Declare the enumeration list
				}
			}
		#END_IF

(*				RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY1,			// Declare which device the parameter is for
				'Device Communicating',		// Name the parameter
				'No',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_EQUAL_TO,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_MAINTENANCE,	// Set the notification type - in this case it is a equipment usage notification
				FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'Unknown',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'Unknown',			// Declare what enumeration the parameter should start on
				'Unknown|No|Yes')		// Declare the enumeration list
*)

/*
    RMSRegisterDevice (DISPLAY2,RMS_DISPLAY2_TYPE,RMS_DISPLAY2_BRAND,RMS_DISPLAY2_MODEL)		// Register the Projector's Name, Manufacturer and Model
										// when the RMS Engine Module connects to the RMS Server.

    RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY2,			// Declare which device the parameter is for
				'Current Input',		// Name the parameter
				'No Input',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_NONE,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_EQUIPMENT_USAGE,	// Set the notification type - in this case it is a equipment usage notification
				FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'No Input',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'No Input',			// Declare what enumeration the parameter should start on
				'No Input|DVI Input|VGA Input|
				Component Input|S-Video Input|
				Composite Input|HDMI Input')		// Declare the enumeration list


RMSRegisterDeviceNumberParamWithUnits(DISPLAY2,			//dvDPS,
				'Filter Time',			//cName,
				'Hours',			//cUnits,
				1000,				//slThreshold,
				RMS_COMP_GREATER_THAN,		//nThresholdCompare,
				RMS_STAT_MAINTENANCE,		//nThresholdStatus,
				true,				//bCanReset,
				0,				//slResetValue,
				RMS_PARAM_SET,			//nInitialOp,
				DISPLAY_FILTER_TIME[2][FILTER_HOURS],			//slInitial,
				0,				//slMin,
				5000)				//slMax)

RMSRegisterDeviceNumberParamWithUnits(DISPLAY2,			//dvDPS,
				'Total Run Time',		//cName,
				'Hours',			//cUnits,
				2000,				//slThreshold,
				RMS_COMP_GREATER_THAN,		//nThresholdCompare,
				RMS_STAT_EQUIPMENT_USAGE,	//nThresholdStatus,
				true,				//bCanReset,
				0,				//slResetValue,
				RMS_PARAM_SET,			//nInitialOp,
				DISPLAY_FILTER_TIME[2][TOTAL_HOURS],			//slInitial,
				0,				//slMin,
				30000)				//slMax)

    RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY2,			// Declare which device the parameter is for
				'Display Error',		// Name the parameter
				'No Error',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_NOT_EQUAL_TO,		// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_MAINTENANCE,		// Set the notification type - in this case it is a equipment usage notification
				TRUE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'No Error',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'No Error',			// Declare what enumeration the parameter should start on
				'No Error|General Error|Lamp Error|Temperature Error|
				Cover Error|Comms Error')			// Declare the enumeration list
(*
				RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY2,			// Declare which device the parameter is for
				'Device Communicating',		// Name the parameter
				'No',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_EQUAL_TO,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_MAINTENANCE,	// Set the notification type - in this case it is a equipment usage notification
				FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'Unknown',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'Unknown',			// Declare what enumeration the parameter should start on
				'Unknown|No|Yes')		// Declare the enumeration list
*)

    RMSRegisterDevice (DISPLAY3,RMS_DISPLAY3_TYPE,RMS_DISPLAY3_BRAND,RMS_DISPLAY3_MODEL)		// Register the Projector's Name, Manufacturer and Model
										// when the RMS Engine Module connects to the RMS Server.

    RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY3,			// Declare which device the parameter is for
				'Current Input',		// Name the parameter
				'No Input',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_NONE,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_EQUIPMENT_USAGE,	// Set the notification type - in this case it is a equipment usage notification
				FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'No Input',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'No Input',			// Declare what enumeration the parameter should start on
				'No Input|DVI Input|VGA Input|
				Component Input|S-Video Input|
				Composite Input|HDMI Input')		// Declare the enumeration list

RMSRegisterDeviceNumberParamWithUnits(DISPLAY3,			//dvDPS,
				'Filter Time',			//cName,
				'Hours',			//cUnits,
				1000,				//slThreshold,
				RMS_COMP_GREATER_THAN,		//nThresholdCompare,
				RMS_STAT_MAINTENANCE,		//nThresholdStatus,
				true,				//bCanReset,
				0,				//slResetValue,
				RMS_PARAM_SET,			//nInitialOp,
				DISPLAY_FILTER_TIME[3][FILTER_HOURS],			//slInitial,
				0,				//slMin,
				5000)				//slMax)

RMSRegisterDeviceNumberParamWithUnits(DISPLAY3,			//dvDPS,
				'Total Run Time',		//cName,
				'Hours',			//cUnits,
				20000,				//slThreshold,
				RMS_COMP_GREATER_THAN,		//nThresholdCompare,
				RMS_STAT_EQUIPMENT_USAGE,	//nThresholdStatus,
				true,				//bCanReset,
				0,				//slResetValue,
				RMS_PARAM_SET,			//nInitialOp,
				DISPLAY_FILTER_TIME[3][TOTAL_HOURS],			//slInitial,
				0,				//slMin,
				50000)				//slMax)

(*RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY3,			// Declare which device the parameter is for
				'Display Error',		// Name the parameter
				'No Error',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_NOT_EQUAL_TO,		// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_MAINTENANCE,		// Set the notification type - in this case it is a equipment usage notification
				TRUE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'No Error',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'No Error',			// Declare what enumeration the parameter should start on
				'No Error|General Error|Lamp Error|Temperature Error|
				Cover Error|Comms Error')			// Declare the enumeration list
*)
(*				RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(DISPLAY3,			// Declare which device the parameter is for
				'Device Communicating',		// Name the parameter
				'No',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_EQUAL_TO,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_MAINTENANCE,	// Set the notification type - in this case it is a equipment usage notification
				FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'Unknown',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'Unknown',			// Declare what enumeration the parameter should start on
				'Unknown|No|Yes')		// Declare the enumeration list*)
*/
RMSRegisterDeviceIndexParam	(dvSystem,			//dvDPS,
				'Motion Detection',		//cName,
				1,				//nThreshold,
				RMS_COMP_GREATER_THAN,		//nThresholdCompare,
				RMS_STAT_SECURITY_ERR,		//nThresholdStatus,
				true,				//bCanReset,
				0,				//nResetValue,
				RMS_PARAM_SET,			//nInitialOp,
				0,				//nInitial,
				'No Motion|Motion|Out of Hours Motion')		//cEnumList)

RMSRegisterDeviceIndexParam	(dvSystem,			//dvDPS,
				'Computer Status',		//cName,
				0,				//nThreshold,
				RMS_COMP_NONE,		//nThresholdCompare,
				RMS_STAT_MAINTENANCE,		//nThresholdStatus,
				FALSE,				//bCanReset,
				0,				//nResetValue,
				RMS_PARAM_SET,			//nInitialOp,
				PC_POWER,			//nInitial,
				'Power OFF|Power ON')		//cEnumList)


    // add basic devices on/offline any AMX type ip device
    RMSRegisterDevice (TP,RMS_TP_TYPE,'AMX',RMS_TP_MODEL)           	// Register the Touch Panel's Name, Manufacturer and Model when the RMS Engine Module connects to the RMS Server.
    RMSRegisterDevice (DVD,RMS_DVD_TYPE,RMS_DVD_BRAND,RMS_DVD_MODEL)		// Register the DVD's Name, Manufacturer and Model
	//    RMSRegisterDevice (AUTO_PATCH_SW, RMS_SWITCHER_TYPE,RMS_SWITCHER_BRAND,RMS_SWITCHER_MODEL)            // Register the Touch Panel's Name, Manufacturer and Model when the RMS Engine Module connects to the RMS Server.

		#IF_DEFINED GROUP_700
		RMSRegisterDevice (POLYCOM, 'Video Conference','Polycom',RMS_VC_MODEL)     // Register the Touch Panel's Name, Manufacturer and Model when the RMS Engine Module connects to the RMS Server.
		RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
				(POLYCOM,			// Declare which device the parameter is for
				'Device Communicating',		// Name the parameter
				'No',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
				RMS_COMP_EQUAL_TO,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
				RMS_STAT_MAINTENANCE,	// Set the notification type - in this case it is a equipment usage notification
				FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
				'Unknown',			// Set the reset value (but because we cannot reset, set this value to No Input)
				RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
				'Unknown',			// Declare what enumeration the parameter should start on
				'Unknown|No|Yes')		// Declare the enumeration list
		#END_IF

		#IF_DEFINED TESIRA
			RMSRegisterDevice (BIAMP_DSP, RMS_DSP_TYPE,RMS_DSP_BRAND,RMS_DSP_MODEL)     	// Register the Touch Panel's Name, Manufacturer and Model when the RMS Engine Module connects to the RMS Server.
			RMSRegisterDeviceEnumParam					// An enumeration parameter is simply a text list
					(BIAMP_DSP,			// Declare which device the parameter is for
					'Device Communicating',		// Name the parameter
					'No',			// Set the inital threshold of the parameter - in this case we are not using a threshold, but we will use 'No Input' anyway
					RMS_COMP_EQUAL_TO,			// Set the conditon that has to be met for an error to occur - in this case there is No threshold
					RMS_STAT_MAINTENANCE,	// Set the notification type - in this case it is a equipment usage notification
					FALSE,				// Declare whether the error condition can be reset on the MeetingManager Admin page
					'Unknown',			// Set the reset value (but because we cannot reset, set this value to No Input)
					RMS_PARAM_SET,			// Declare the initial operation of this parameter - in this case we are going to set the parameter
					'Unknown',			// Declare what enumeration the parameter should start on
					'Unknown|No|Yes')		// Declare the enumeration list*)
	//    RMSRegisterDevice (vdvCapHardwarePort,'Capture Device','Echo','Echo 360')     // Register the Touch Panel's Name, Manufacturer and Model when the RMS Engine Module connects to the RMS Server.ch Panel's Name, Manufacturer and Model when the RMS Engine Module connects to the RMS Server.
		#END_IF
}


// FEEDBACK FROM RMS INTERFACE TO AMX PROGRAM
DEFINE_FUNCTION RMSDevMonSetParamCallback(DEV dvDPS, CHAR cName[], CHAR cValue[])
{
   select
	{
	active(dvDPS = DISPLAY1):
	    {
	    select
		{
		ACTIVE (cName == 'Filter Time'):
		    {
		    DISPLAY_FILTER_TIME[1][FILTER_MINUTES] = 0
		    DISPLAY_FILTER_TIME[1][FILTER_HOURS] = ATOI(cValue)
		    }
    		ACTIVE (cName == 'Total Run Time'):
		    {
		    DISPLAY_FILTER_TIME[1][TOTAL_HOURS] = ATOI(cValue)
		    }

		}
	    }
	}
   select
	{
	active(dvDPS = DISPLAY2):
	    {
	    select
		{
		ACTIVE (cName == 'Filter Time'):
		    {
		    DISPLAY_FILTER_TIME[2][FILTER_MINUTES] = 0
		    DISPLAY_FILTER_TIME[2][FILTER_HOURS] = ATOI(cValue)
		    }
		ACTIVE (cName == 'Total Run Time'):
		    {
		    DISPLAY_FILTER_TIME[2][TOTAL_HOURS] = ATOI(cValue)
		    }
		}
	    }
	}
   select
	{
	active(dvDPS = DISPLAY3):
	    {
	    select
		{
		ACTIVE (cName == 'Filter Time'):
		    {
		    DISPLAY_FILTER_TIME[3][FILTER_MINUTES] = 0
		    DISPLAY_FILTER_TIME[3][FILTER_HOURS] = ATOI(cValue)
		    }
		ACTIVE (cName == 'Total Run Time'):
		    {
		    DISPLAY_FILTER_TIME[3][TOTAL_HOURS] = ATOI(cValue)
		    }
		}
	    }
	}
(* select{
  //This section allows the AMX master to receive information from the RMS server when someone clicks
  //the 'Reset' icon.  We catch this event and use it to force the Lectopia module to query the server
  //for new venue details.  This may be useful if your Digitiser/Capture Appliance has picked up a
  //different DHCP address and you want your venue to update its details immediatly.
  active(dvDPS == vdvLect):{
   if(cName == 'Capture Platform State'){
    if(cValue == 'Refresh')
     send_command vdvLect,'SERVER REFRESH'
   }
  }
 }*)
}

(***********************************************************)
(*                 STARTUP CODE GOES BELOW                 *)
(***********************************************************)
DEFINE_START

(***********************************************************)
(*                 MODULE CODE GOES BELOW                  *)
(***********************************************************)

// use projector parameters
DEFINE_MODULE 'RMSProjectorMod' mdlProj01(DISPLAY1,		// You need this Module to tell MeetingManager
					  DISPLAY1,(*only virtual if AMX Module*)		// that you want to track Projector usage.
					  vdvRMSEngine)		// You have to delcare 1 module instance per projector

DEFINE_MODULE 'RMSProjectorMod' mdlProj02(DISPLAY2,		// You need this Module to tell MeetingManager
					  DISPLAY2,(*only virtual if AMX Module*)		// that you want to track Projector usage.
					  vdvRMSEngine)		// You have to delcare 1 module instance per projector

DEFINE_MODULE 'RMSProjectorMod' mdlProj03(DISPLAY3,		// You need this Module to tell MeetingManager
					  DISPLAY3,(*only virtual if AMX Module*)		// that you want to track Projector usage.
					  vdvRMSEngine)		// You have to delcare 1 module instance per projector
// needed every time
DEFINE_MODULE 'RMSEngineMod' mdlRMSEng(vdvRMSEngine,		// This i-ConnectLinx Module always has to be defined, otherwise
                                       dvRMSSocket,		// your Master will not communicate with MeetingManager
                                       vdvCLActions)
// timers on device selection
DEFINE_MODULE 'RMSSrcUsageMod' mdlSrcUsage(vdvRMSEngine,	// You need this Module to tell MeetingManager
                                           vdvCLActions)	// that you want to track equipment usage.

(*// Help module IF TOUCH PANEL AND HELP SCREENS ON DIFFERENT TP PORT
DEFINE_MODULE 'RMSHelpUIMod'  mdlRMSHelpUI(vdvRMSEngine,
					  vdvTP_help_tp,(*different port from main TP*)
					  vdvTP)        (*has to be port 1 TP*)
*)
//Transport monitoring
DEFINE_MODULE 'RMSTransportMod' mdlDVD1(DVD,
					DVD,
					vdvRMSEngine)

// used for macros
DEFINE_MODULE 'i!-ConnectLinxEngineMod' mdlConnectLinx1(vdvClactions)

//DEFINE_MODULE 'RMSBasicDeviceMod' RMSBas1 (AUTO_PATCH_SW,AUTO_PATCH_SW,vdvRMSEngine)
#IF_DEFINED GROUP_700
	DEFINE_MODULE 'RMSBasicDeviceMod' RMSBas2 (POLYCOM,POLYCOM,vdvRMSEngine)
#END_IF
#IF_DEFINED TESIRA
	DEFINE_MODULE 'RMSBasicDeviceMod' RMSBas3 (BIAMP_DSP,BIAMP_DSP,vdvRMSEngine)
#END_IF
//DEFINE_MODULE 'RMSBasicDeviceMod' RMSBas4 (vdvCapHardwarePort,vdvCapHardwarePort,vdvRMSEngine)


(***********************************************************)
(*                  THE EVENTS GOES BELOW                  *)
(***********************************************************)
DEFINE_EVENT

(*******************************************)
(* DATA: RMS Engine                        *)
(*******************************************)
(*DATA_EVENT[DISPLAY1]
{
	STRING:
	{
		RMSChangeEnumParam(DISPLAY1,'Device Communicating','Yes')
		nDisplayStringsSent[1] = 0
	}
}
DATA_EVENT[DISPLAY2]
{
	STRING:
	{
		RMSChangeEnumParam(DISPLAY2,'Device Communicating','Yes')
		nDisplayStringsSent[2] = 0
	}
}*)
(*DATA_EVENT[DISPLAY3]
{
	STRING:
	{
		RMSChangeEnumParam(DISPLAY3,'Device Communicating','Yes')
		nDisplayStringsSent[3] = 0
	}
}*)
(*DATA_EVENT[dvDSP]
{
	STRING:
	{
		RMSChangeEnumParam(dvDSP,'Device Communicating','Yes')
		nDSPStringsSent = 0
	}
}*)

DATA_EVENT[POLYCOM]
{
	STRING:
	{
		RMSChangeEnumParam(POLYCOM,'Device Communicating','Yes')
		nVCStringsSent = 0
	}
}

DATA_EVENT [vdvRMSEngine]
{
    ONLINE:
    {
	RMSSetServer (RMS_SERVER_IP)			// This function MUST be present, otherwise the Master will not connect to the RMS Server.

    RMSSetCommunicationTimeout (TP,0)   		// Because this device is a Touch Panel, we are No to concerned with comms timeouts, so the timeout has been set to 0

	// must reply to all strings !!
	RMSSetCommunicationTimeout (DISPLAY1,2000)	// poll time
	RMSSetCommunicationTimeout (DISPLAY2,2000)	// poll time
	RMSSetCommunicationTimeout (DISPLAY3,2000)	// poll time
//	RMSSetCommunicationTimeout (AUTO_PATCH_SW,2000)	// poll time
	#IF_DEFINED GROUP_700
		RMSSetCommunicationTimeout (POLYCOM,2000)	// poll time
	#END_IF
	#IF_DEFINED TESIRA
		RMSSetCommunicationTimeout (BIAMP_DSP,2000)	// poll time
	#END_IF
//	RMSSetCommunicationTimeout (vdvCapHardwarePort,2000)	// poll time
	// multiple input state
	RMSSetMultiSource(TRUE)
	RMSDevMonRegisterCallback()


    }
}








BUTTON_EVENT[vdvTP,0]
BUTTON_EVENT[dvIO,PIR]
{
 PUSH:
 {
 CANCEL_WAIT 'Motion Wait'
 IF(OUT_OF_HOURS){MOVEMENT = 2}
  ELSE{MOVEMENT = 1}
 WAIT 100
 {
 RMSChangeIndexParam (dvSystem,'Motion Detection',MOVEMENT)
 }
 WAIT MOTION_TIMEOUT 'Motion Wait'
  {
  MOVEMENT = 0
  RMSChangeIndexParam (dvSystem,'Motion Detection',MOVEMENT)
  }
 }
}




CHANNEL_EVENT[dvIO,PC_STATUS_IO]
{
 ON:
 {
 RMSChangeIndexParam  (dvSystem,'Computer Status',1)
 }
 OFF:
 {
  RMSChangeIndexParam  (dvSystem,'Computer Status',0)
 }
}

DATA_EVENT[TP]
{
online:
 {
 RMSNetlinxDeviceOnline(TP,RMS_TP_TYPE)
 }
offline:
 {
 RMSNetlinxDeviceOffline(TP)
 }
}


//This is where we update RMS with the current state of the Lecotpia module.  Note the way we handle Stealth mode so that
//such Stealth usage is recorded in the RMS server.
(*channel_event[vdvLectStates]{
 on:{
  if([vdvRMSEngine,250]){
   switch(channel.channel){
    case 241:  //Idle
     if([vdvLect,249])  //Is the module performing a Stealth recording?
      RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Stealth')
     else
      RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Idle')
     break;
    case 242:  //Paused
     RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Paused')
     break;
    case 243:  //Trigger
     RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Trigger')
     break;
    case 244:  //Recording
     RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Recording')
     break;
    case 245:  //Finalizing
     RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Finalizing')
     break;
   }
  }
 }
}*)

//The vdvLect device has just established a connection to the capture platform.  By registering the device with RMS at this point we can
//deploy the Lectopia code to non-Lectopia venues, and the Lecotpia stats will only come through to RMS if the venue is indeed a Lectopia
//venue.  Non-Lectopia venues will not have any misleading Lectopia information in RMS.
(*channel_event[vdvLect,254]{  //Capture platform online/offline channel
 on:{
  wait_until([vdvRMSEngine,250]) 'RMS ready for Lectopia info'{
   //Register module with RMS.
   RMSRegisterDevice(vdvLect,'Capture Device','Echo 360','Safe Capture HD')
   RMSRegisterDeviceStringParam(vdvLect,'Module Version','???',RMS_COMP_NONE,RMS_STAT_NOT_ASSIGNED,false,'???',RMS_PARAM_UNCHANGED,'')
   RMSRegisterDeviceStringParam(vdvLect,'Capture Platform IP','',RMS_COMP_NONE,RMS_STAT_EQUIPMENT_USAGE,false,'???',RMS_PARAM_UNCHANGED,'')
   RMSRegisterDeviceStringParam(vdvLect,'Capture Platform Type','',RMS_COMP_NONE,RMS_STAT_EQUIPMENT_USAGE,false,'???',RMS_PARAM_UNCHANGED,'')
   RMSRegisterStockStringParam(vdvLect,'Capture Platform State','',true,'Offline',RMS_COMP_EQUAL_TO,RMS_STAT_MAINTENANCE,true,'Refresh',RMS_PARAM_UNCHANGED,'')

   if([vdvLect,241]){
    if([vdvLect,249])
     RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Stealth')
    else
     RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Idle')
   }
   else if([vdvLect,242]) RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Paused')
   else if([vdvLect,243]) RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Trigger')
   else if([vdvLect,244]) RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Recording')
   else if([vdvLect,245]) RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Finalizing')

   //Lets invoke some replies which get caught in the string handler below.
   send_command vdvLect,'VERSION?'
   send_command vdvLect,'DEVICE?'
   send_command vdvLect,'DEVICE TYPE?'
   send_command vdvLect,'STEALTH?'
  }
 }
 off:{
  cancel_wait 'RMS ready for Lectopia info'
  if([vdvRMSEngine,250])  //Capture platform has gone offline
   RMSChangeStringParam(vdvLect,'Capture Platform State',RMS_PARAM_SET,'Offline')
 }
}

//In the above registration we sent a few queries to the Lectopia module to get details of the venue equipment.
//Here we handle the replies from the Lectopia module and feed the information to RMS.  Whenever the module
//obtains new capture platform details from the server (IP address, platform type, etc) the module generates
//some string feedback for the new details.  This is also caught below and RMS is updated accordingly.
data_event[vdvLect]{
 string:{
  stack_var char temp[40]

  if([vdvRMSEngine,250]){  //If connection to RMS is up and okay
   if(find_string(data.text,'VERSION=',1)){  //This is the version of the Lectopia module, not the capture platform or Lectopia server.
    temp = "data.text"                       //It can be useful to put this info into RMS so you can check that all venues are running
    remove_string(temp,'VERSION=',1)         //the same module version.
    if(length_array(temp) > 0)
     RMSChangeStringParam(vdvLect,'Module Version',RMS_PARAM_SET,temp)
   }
   else if(find_string(data.text,'DEVICE=',1)){  //Capture platform IP address
    temp = "data.text"
    remove_string(temp,'DEVICE=',1)
    if(length_array(temp) > 0)
     RMSChangeStringParam(vdvLect,'Capture Platform IP',RMS_PARAM_SET,temp)
   }
   else if(find_string(data.text,'DEVICE TYPE=',1)){
    temp = "data.text"
    remove_string(temp,'DEVICE TYPE=',1)
    if(length_array(temp) > 0)
     RMSChangeStringParam(vdvLect,'Capture Platform Type',RMS_PARAM_SET,temp)
   }
  }
 }
}*)








(*******************************************)
(* DATA: i!-ConnectLinx Engine             *)
(*******************************************)
DATA_EVENT[vdvCLActions]
{
    STRING:
    {
	STACK_VAR
	CHAR    cTemp[1000]
	CHAR    cTrash[10]
	INTEGER nId
	// Look for arguments
	IF (LEFT_STRING(DATA.TEXT,3) = 'ARG')
	{
	  // Get arg ID
	  cTemp = DATA.Text
	  cTrash = REMOVE_STRING(cTemp,'ARG',1)
	  nId = ATOI(cTemp)
	  cTrash = REMOVE_STRING(cTemp,'-',1)
	  // Store it if we have room
	  IF (MAX_LENGTH_ARRAY(acStringEnumArgValues) >= nId)
	    acStringEnumArgValues [nId] = cTemp
	}
    }
    ONLINE:
    {
	SEND_COMMAND DATA.DEVICE,"'SET ROOM INFO-',LECTOPIA_VENUE"			// Sets the Room Info


	SEND_COMMAND DATA.DEVICE,'ADD FOLDER-Source Select'                                  	// Creates a folder called System Controls in the "Build Preset" Window when you click "Setup Preset" Button in the "Modify Appointment Details" Window on the MeetingManager User Pages
	SEND_COMMAND DATA.DEVICE,'ADD FOLDER-Room Control'                                      // Creates a folder called System Controls in the "Build Preset" Window when you click "Setup Preset" Button in the "Modify Appointment Details" Window on the MeetingManager User Pages
	SEND_COMMAND DATA.DEVICE,'ADD FOLDER-Lights,Room Control'                         	// Creates a folder called System Controls in the "Build Preset" Window when you click "Setup Preset" Button in the "Modify Appointment Details" Window on the MeetingManager User Pages

	SEND_COMMAND DATA.DEVICE,'ADD ACTION-1,Select VCR,Display VCR,Source Select'		// VCR
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-2,Select DVD,Display DVD,Source Select'		// DVD
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-3,Select TV,Display TV via VCR,Source Select'	// STB
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-4,Select DVD_R'					// DVD_R
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-5,Select FOX'					// FOX
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-6,Select VC,Video Conference,Source Select'	// VC
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-7,Select DOC CAM,Doc Cam Input,Source Select'	// DOC CAM
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-8,Select CD,CD Audio, Source Select'		// CD
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-9,Select AUX AV,Aux AV Input,Source Select' 	// PC
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-10,Select COMPUTER,Computer,Source Select' 	// PC
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-11,Select LAPTOP,Laptop,Source Select'		// LT 1
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-12,Select HDMI,HDMI,Source Select'	// LT 2


	// LIGHTS
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-60,Lights ON,ALL ON,Lights,Room Control'
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-61,Lights 75%,Lights 75%,Lights,Room Control'
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-62,Lights 25%,Lights 25%,Lights,Room Control'
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-63,Lights OFF,ALL OFF,Lights,Room Control'
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-64,Lights Video,ALL OFF,Lights,Room Control'
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-65,Lights Board,Board Toggle,Lights,Room Control'

	// SCREENS
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-70,Blinds Up,Motorised blinds Up,Room Control'
	//SEND_COMMAND DATA.DEVICE,'ADD ACTION-72,Blinds Down,Motorised blinds Down,Room Control'



	// PROJECTOR POWER
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-85,SYSTEM OFF,Sytem Shutdown,Room Control'

	SEND_COMMAND DATA.DEVICE,'ADD ACTION-86,Display 1 Off only,Display 1 off only,Room Control'
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-87,Display 2 Off only,Display 2 off only,Room Control'
	SEND_COMMAND DATA.DEVICE,'ADD ACTION-88,Display 3 Off only,Display 3 off only,Room Control'
    }
}



BUTTON_EVENT [vdvCLActions,1]	{PUSH:{DO_PUSH(vdvTP,1)		SEND_STRING 0,'RMS VCR Select'}}
BUTTON_EVENT [vdvCLActions,2]	{PUSH:{DO_PUSH(vdvTP,2)		SEND_STRING 0,'RMS DVD Select'}}
BUTTON_EVENT [vdvCLActions,3]	{PUSH:{DO_PUSH(vdvTP,3)		SEND_STRING 0,'RMS TV Select'}}
BUTTON_EVENT [vdvCLActions,4]	{PUSH:{DO_PUSH(vdvTP,4)		SEND_STRING 0,'RMS DVD-R Select'}}
BUTTON_EVENT [vdvCLActions,5]	{PUSH:{DO_PUSH(vdvTP,5)		SEND_STRING 0,'RMS Fox Select'}}
BUTTON_EVENT [vdvCLActions,6]	{PUSH:{DO_PUSH(vdvTP,6)		SEND_STRING 0,'RMS Select VC'}}
BUTTON_EVENT [vdvCLActions,7]	{PUSH:{DO_PUSH(vdvTP,7)		SEND_STRING 0,'RMS Doc Camera Select'}}
BUTTON_EVENT [vdvCLActions,8]	{PUSH:{DO_PUSH(vdvTP,8)		SEND_STRING 0,'RMS CD Select'}}
BUTTON_EVENT [vdvCLActions,9]	{PUSH:{DO_PUSH(vdvTP,9)		SEND_STRING 0,'RMS Aux AV Select'}}
BUTTON_EVENT [vdvCLActions,10]	{PUSH:{DO_PUSH(vdvTP,10)		SEND_STRING 0,'RMS PC Select'}}
BUTTON_EVENT [vdvCLActions,11]	{PUSH:{DO_PUSH(vdvTP,11)		SEND_STRING 0,'RMS Laptop Select'}}
BUTTON_EVENT [vdvCLActions,12]	{PUSH:{DO_PUSH(vdvTP,12)		SEND_STRING 0,'RMS Laptop 2 Select'}}

//BUTTON_EVENT [vdvCLActions,15]	{PUSH:{DO_PUSH(TP_ROOM1,15)		SEND_STRING 0,'RMS Send to Left'}}
//BUTTON_EVENT [vdvCLActions,16]	{PUSH:{DO_PUSH(TP_ROOM1,16)		SEND_STRING 0,'RMS Send to Right'}}

BUTTON_EVENT [vdvCLActions,60]	{PUSH:{DO_PUSH(vdvTP,60)		SEND_STRING 0,'RMS Lights 100%'}}
BUTTON_EVENT [vdvCLActions,61]	{PUSH:{DO_PUSH(vdvTP,61)		SEND_STRING 0,'RMS Lights 75%'}}
BUTTON_EVENT [vdvCLActions,62]	{PUSH:{DO_PUSH(vdvTP,62)		SEND_STRING 0,'RMS Lights 25%'}}
BUTTON_EVENT [vdvCLActions,63]	{PUSH:{DO_PUSH(vdvTP,63)		SEND_STRING 0,'RMS Lights 0%'}}
BUTTON_EVENT [vdvCLActions,64]	{PUSH:{DO_PUSH(vdvTP,64)		SEND_STRING 0,'RMS Lights Video'}}
BUTTON_EVENT [vdvCLActions,65]	{PUSH:{DO_PUSH(vdvTP,65)		SEND_STRING 0,'RMS Bord Lights'}}

BUTTON_EVENT [vdvCLActions,70]	{PUSH:{DO_PUSH(vdvTP,70) 	SEND_STRING 0,'RMS Screen up'}}
BUTTON_EVENT [vdvCLActions,71]	{PUSH:{DO_PUSH(vdvTP,71) 	SEND_STRING 0,'RMS Screen stop'}}
BUTTON_EVENT [vdvCLActions,72]	{PUSH:{DO_PUSH(vdvTP,72) 	SEND_STRING 0,'RMS Screen down'}}

BUTTON_EVENT [vdvCLActions,73]	{PUSH:{DO_PUSH(vdvTP,73) 	SEND_STRING 0,'RMS Screen 2 up'}}
BUTTON_EVENT [vdvCLActions,74]	{PUSH:{DO_PUSH(vdvTP,74) 	SEND_STRING 0,'RMS Screen 2 stop'}}
BUTTON_EVENT [vdvCLActions,75]	{PUSH:{DO_PUSH(vdvTP,75) 	SEND_STRING 0,'RMS Screen 2 dn'}}

BUTTON_EVENT [vdvCLActions,76]	{PUSH:{DO_PUSH(vdvTP,76) 	SEND_STRING 0,'RMS Screen 3 up'}}
BUTTON_EVENT [vdvCLActions,77]	{PUSH:{DO_PUSH(vdvTP,77) 	SEND_STRING 0,'RMS Screen 3 stop'}}
BUTTON_EVENT [vdvCLActions,78]	{PUSH:{DO_PUSH(vdvTP,78) 	SEND_STRING 0,'RMS Screen 3 dn'}}

BUTTON_EVENT [vdvCLActions,79]	{PUSH:{DO_PUSH(vdvTP,79) 	SEND_STRING 0,'RMS Screen 4 up'}}
BUTTON_EVENT [vdvCLActions,80]	{PUSH:{DO_PUSH(vdvTP,80) 	SEND_STRING 0,'RMS Screen 4 stop'}}
BUTTON_EVENT [vdvCLActions,81]	{PUSH:{DO_PUSH(vdvTP,81) 	SEND_STRING 0,'RMS Screen 4 dn'}}

BUTTON_EVENT [vdvCLActions,85]	{PUSH:{DO_PUSH(vdvTP,85) 	SEND_STRING 0,'RMS System Off'}}

BUTTON_EVENT [vdvCLActions,86]	{PUSH:{DO_PUSH(vdvTP,86) 	SEND_STRING 0,'RMS Display 1 Off'}}
BUTTON_EVENT [vdvCLActions,87]	{PUSH:{DO_PUSH(vdvTP,87) 	SEND_STRING 0,'RMS Display 2 Off'}}
BUTTON_EVENT [vdvCLActions,88]	{PUSH:{DO_PUSH(vdvTP,88) 	SEND_STRING 0,'RMS Display 3 Off'}}


DATA_EVENT [vdvDISPLAY1]
 {
 STRING:
  {
  if (data.text == 'WARM UP')	{}
  if (data.text == 'COOL DOWN')	{}
  if (data.text == 'POWER ON')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'No Input') RMSSetDevicePower (DISPLAY1,TRUE)}
  if (data.text == 'POWER OFF')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'No Input') RMSSetDevicePower (DISPLAY1,FALSE)}

  if (data.text == 'VIDEO 1')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'Composite Input')}	// VP V1
  if (data.text == 'VIDEO 2')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'S-Video Input')}	// VP V2
  if (data.text == 'VIDEO 3')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'Component Input')}	// VP V4
  if (data.text == 'RGB 1')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'VGA Input')}	// VP R1
  if (data.text == 'RGB 2')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'VGA Input')}	// VP R2
  if (data.text == 'RGB 3')	{RMSChangeEnumParam (DISPLAY1, 'Current Input', 'HDMI Input')}	// VP R2

   if (FIND_STRING (DATA.TEXT, 'LAMP =',1))
    {
    REMOVE_STRING (DATA.TEXT,'LAMP =',1)
    if(atoi(data.text) < 5000 ){RMSSetLampHours (DISPLAY1,ATOI(DATA.TEXT))}
	(*				DISPLAY_FILTER_TIME[1][TOTAL_HOURS] = atoi(data.text)

				if(atoi(data.text) < 30000 )
				 {
					RMSChangeNumberParam (DISPLAY1,'Total Run Time',RMS_PARAM_SET,ATOI(DATA.TEXT))
				 }
*)
    }

	if (FIND_STRING (DATA.TEXT, 'ERROR:',1))
    {
    REMOVE_STRING (DATA.TEXT,'ERROR:',1)
    IF(DATA.TEXT = 'NONE')		{RMSChangeEnumParam (DISPLAY1, 'Display Error', 'No Error')}
    ELSE IF(DATA.TEXT = 'LAMP')		{RMSChangeEnumParam (DISPLAY1, 'Display Error', 'Lamp Error')}
    ELSE IF(DATA.TEXT = 'COVER')	{RMSChangeEnumParam (DISPLAY1, 'Display Error', 'Cover Error')}
    ELSE IF(DATA.TEXT = 'TEMP')		{RMSChangeEnumParam (DISPLAY1, 'Display Error', 'Temperature Error')}
    ELSE IF(DATA.TEXT = 'COMMS')	{RMSChangeEnumParam (DISPLAY1, 'Display Error', 'Comms Error')}    }
  }
 }

DATA_EVENT [vdvDISPLAY2]
 {
 STRING:
  {
  if (data.text == 'WARM UP')	{}
  if (data.text == 'COOL DOWN')	{}
  if (data.text == 'POWER ON')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'No Input') RMSSetDevicePower (DISPLAY2,TRUE)}
  if (data.text == 'POWER OFF')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'No Input') RMSSetDevicePower (DISPLAY2,FALSE)}

  if (data.text == 'VIDEO 1')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'Composite Input')}	// VP V1
  if (data.text == 'VIDEO 2')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'S-Video Input')}	// VP V2
  if (data.text == 'VIDEO 3')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'Component Input')}	// VP V4
  //if (data.text == 'RGB 1')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'VGA Input')}	// VP R1
  //if (data.text == 'RGB 2')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'VGA Input')}	// VP R2
  if (data.text == 'RGB 4')	{RMSChangeEnumParam (DISPLAY2, 'Current Input', 'HDMI Input')}	// VP R2

   if (FIND_STRING (DATA.TEXT, 'LAMP =',1))
    {
    REMOVE_STRING (DATA.TEXT,'LAMP =',1)
    if(atoi(data.text) < 5000 ){RMSSetLampHours (DISPLAY2,ATOI(DATA.TEXT))}
	(*				DISPLAY_FILTER_TIME[2][TOTAL_HOURS] = atoi(data.text)

				if(atoi(data.text) < 30000 )
				 {
					RMSChangeNumberParam (DISPLAY2,'Total Run Time',RMS_PARAM_SET,ATOI(DATA.TEXT))
				 }
*)
    }

	if (FIND_STRING (DATA.TEXT, 'ERROR:',1))
    {
    REMOVE_STRING (DATA.TEXT,'ERROR:',1)
    IF(DATA.TEXT = 'NONE')		{RMSChangeEnumParam (DISPLAY2, 'Display Error', 'No Error')}
    ELSE IF(DATA.TEXT = 'LAMP')		{RMSChangeEnumParam (DISPLAY2, 'Display Error', 'Lamp Error')}
    ELSE IF(DATA.TEXT = 'COVER')	{RMSChangeEnumParam (DISPLAY2, 'Display Error', 'Cover Error')}
    ELSE IF(DATA.TEXT = 'TEMP')		{RMSChangeEnumParam (DISPLAY2, 'Display Error', 'Temperature Error')}
    ELSE IF(DATA.TEXT = 'COMMS')	{RMSChangeEnumParam (DISPLAY2, 'Display Error', 'Comms Error')}    }
  }
 }

DATA_EVENT [vdvDISPLAY3]
 {
 STRING:
  {
  if (data.text == 'WARM UP')	{}
  if (data.text == 'COOL DOWN')	{}
  if (data.text == 'POWER ON')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'No Input') RMSSetDevicePower (DISPLAY3,TRUE)}
  if (data.text == 'POWER OFF')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'No Input') RMSSetDevicePower (DISPLAY3,FALSE)}

  if (data.text == 'VIDEO 1')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'Composite Input')}	// VP V1
  if (data.text == 'VIDEO 2')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'S-Video Input')}	// VP V2
  if (data.text == 'VIDEO 3')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'Component Input')}	// VP V4
  //if (data.text == 'RGB 1')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'VGA Input')}		// VP R1
  //if (data.text == 'RGB 2')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'VGA Input')}		// VP R2
  if (data.text == 'RGB 3')	{RMSChangeEnumParam (DISPLAY3, 'Current Input', 'HDMI Input')}	// VP R2

   if (FIND_STRING (DATA.TEXT, 'LAMP =',1))
    {
    REMOVE_STRING (DATA.TEXT,'LAMP =',1)
    if(atoi(data.text) < 5000 ){RMSSetLampHours (DISPLAY3,ATOI(DATA.TEXT))}
	(*				DISPLAY_FILTER_TIME[3][TOTAL_HOURS] = atoi(data.text)

				if(atoi(data.text) < 30000 )
				 {
					RMSChangeNumberParam (DISPLAY3,'Total Run Time',RMS_PARAM_SET,ATOI(DATA.TEXT))
				 }
    *)
    }

   if (FIND_STRING (DATA.TEXT, 'ERROR:',1))
    {
    REMOVE_STRING (DATA.TEXT,'ERROR:',1)
    IF(DATA.TEXT = 'NONE')		{RMSChangeEnumParam (DISPLAY3, 'Display Error', 'No Error')}
    ELSE IF(DATA.TEXT = 'LAMP')		{RMSChangeEnumParam (DISPLAY3, 'Display Error', 'Lamp Error')}
    ELSE IF(DATA.TEXT = 'COVER')	{RMSChangeEnumParam (DISPLAY3, 'Display Error', 'Cover Error')}
    ELSE IF(DATA.TEXT = 'TEMP')		{RMSChangeEnumParam (DISPLAY3, 'Display Error', 'Temperature Error')}
    ELSE IF(DATA.TEXT = 'COMMS')	{RMSChangeEnumParam (DISPLAY3, 'Display Error', 'Comms Error')}    }
  }
 }


CHANNEL_EVENT[DVD,PL]{ON:{RMSSetTransportState(DVD,PL)}}
CHANNEL_EVENT[DVD,ST]{ON:{RMSSetTransportState(DVD,ST)}}

CHANNEL_EVENT[dvRELAYS,AMP_POWER]
{
    ON: {RMSSetSystemPower (true)}
    OFF:{RMSSetSystemPower (false)}
}



(***********************************************************)
(*              THE ACTUAL PROGRAM GOES BELOW              *)
(***********************************************************)
DEFINE_PROGRAM

// RMS SOURCE SELECTED
[vdvCLActions,1]   = ((INPUT_STATE[1]  = VCR)&&(DVD_MODE = 1))		// VCR
[vdvCLActions,2]   = ((INPUT_STATE[1]  = DVD)&&(DVD_MODE = 2))		// DVD
[vdvCLActions,4]   = (INPUT_STATE[1]   = DVD_R)				// DVD_R
[vdvCLActions,6]   = (INPUT_STATE[1]   = POLYCOM)			// POLYCOM
[vdvCLActions,7]   = (INPUT_STATE[1]   = DOC_CAM)			// DOC CAM
[vdvCLActions,9]   = (INPUT_STATE[1]   = AUX_AV)			// AUX_AV
[vdvCLActions,10]  = (INPUT_STATE[1]   = COMPUTER)			// PC
[vdvCLActions,11]  = (INPUT_STATE[1]   = LAPTOP1)			// LT 1
[vdvCLActions,12]  = (INPUT_STATE[1]   = LAPTOP2)			// LT 1
[vdvCLActions,13]  = (INPUT_STATE[1]   = MICROSCOPE1)			// Microscope 1
[vdvCLActions,14]  = (INPUT_STATE[1]   = MICROSCOPE2)			// Microscope 2

PC_POWER = [dvIO,PC_STATUS_IO]


// keep comms with polycom
wait 600{
	#IF_DEFINED TESIRA
		SEND_STRING BIAMP_DSP , "'GET 0 IPADDR',$0A"  		// Biamp, REPORT IP ADDRESS4
	#END_IF
 //SEND_STRING DISPLAY3  , "$AA,$00,$00,$00,$00" 		// QUERY INPUT

(* SEND_STRING POLYCOM ,  "'mute near get',$0D,$0A"	// GET MUTE STATE
	nVCStringsSent++
	if(nVCStringsSent = 2)
	{
		nVCStringsSent = 0
		RMSChangeEnumParam(POLYCOM,'Device Communicating','No')
	}
 SEND_STRING dvDSP , "'I'"  // NEXIA
	nDSPStringsSent++
	if(nVCStringsSent = 2)
	{
		nDSPStringsSent = 0
		RMSChangeEnumParam(dvDSP,'Device Communicating','No')
	}*)
(*	nDisplayStringsSent[1]++
	if(nDisplayStringsSent[1] = 2)
	{
		nDisplayStringsSent[1] = 0
		RMSChangeEnumParam(DISPLAY1,'Device Communicating','No')
	}
	nDisplayStringsSent[2]++
	if(nDisplayStringsSent[2] = 2)
	{
		nDisplayStringsSent[2] = 0
		RMSChangeEnumParam(DISPLAY2,'Device Communicating','No')
	}
	(*nDisplayStringsSent[3]++
	if(nDisplayStringsSent[3] = 2)
	{
		nDisplayStringsSent[3] = 0
		RMSChangeEnumParam(DISPLAY3,'Device Communicating','No')
	}*)
*)
 //SEND_STRING SCALER , "'[OSD0]'"  // SCALER DISABLES OSD
 }




// FILTER TIMER
if(display_status[1] = 1)
    {
    wait 600
    {
    DISPLAY_FILTER_TIME[1][FILTER_MINUTES]++

    IF(DISPLAY_FILTER_TIME[1][FILTER_MINUTES] >= 60)
	{
	DISPLAY_FILTER_TIME[1][FILTER_MINUTES] = 0
	DISPLAY_FILTER_TIME[1][FILTER_HOURS]++
	//DISPLAY_FILTER_TIME[1][TOTAL_HOURS]++

	RMSChangeNumberParam (DISPLAY1,'Filter Time',RMS_PARAM_SET,DISPLAY_FILTER_TIME[1][FILTER_HOURS])
	RMSChangeNumberParam (DISPLAY1,'Total Run Time',RMS_PARAM_SET,DISPLAY_FILTER_TIME[1][TOTAL_HOURS])

	}
    }
    }

if(display_status[2] = 1)
    {
    wait 600
    {
    DISPLAY_FILTER_TIME[2][FILTER_MINUTES]++

    IF(DISPLAY_FILTER_TIME[2][FILTER_MINUTES] >= 60)
	{
	DISPLAY_FILTER_TIME[2][FILTER_MINUTES] = 0
	DISPLAY_FILTER_TIME[2][FILTER_HOURS]++
	DISPLAY_FILTER_TIME[2][TOTAL_HOURS]++

	RMSChangeNumberParam (DISPLAY2,'Filter Time',RMS_PARAM_SET,DISPLAY_FILTER_TIME[2][FILTER_HOURS])
	RMSChangeNumberParam (DISPLAY2,'Total Run Time',RMS_PARAM_SET,DISPLAY_FILTER_TIME[2][TOTAL_HOURS])
	}
    }
    }

if(display_status[3] = 1)
    {
    wait 600
    {
    DISPLAY_FILTER_TIME[3][FILTER_MINUTES]++

    IF(DISPLAY_FILTER_TIME[3][FILTER_MINUTES] >= 60)
	{
	DISPLAY_FILTER_TIME[3][FILTER_MINUTES] = 0
	DISPLAY_FILTER_TIME[3][FILTER_HOURS]++
	DISPLAY_FILTER_TIME[3][TOTAL_HOURS]++

	RMSChangeNumberParam (DISPLAY3,'Filter Time',RMS_PARAM_SET,DISPLAY_FILTER_TIME[3][FILTER_HOURS])
	RMSChangeNumberParam (DISPLAY3,'Total Run Time',RMS_PARAM_SET,DISPLAY_FILTER_TIME[3][TOTAL_HOURS])
	}
    }
    }

// SYSTEM OFF AT 10PM
IF (TIME = '22:00:00')
   {
  WAIT 10
       {
	OUT_OF_HOURS = 1
       }
   }
IF (TIME = '06:00:00')
   {
  WAIT 10
       {
	OUT_OF_HOURS = 0
       }
   }
