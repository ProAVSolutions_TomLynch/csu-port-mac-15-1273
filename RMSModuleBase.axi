(*********************************************************************)
(*                                                                   *)
(*             AMX Resource Management Suite  (3.3.33)               *)
(*                                                                   *)
(*********************************************************************)
/*
 *  Legal Notice :
 * 
 *     Copyright, AMX LLC, 2009
 *
 *     Private, proprietary information, the sole property of AMX LLC.  The
 *     contents, ideas, and concepts expressed herein are not to be disclosed
 *     except within the confines of a confidential relationship and only
 *     then on a need to know basis.
 * 
 *     Any entity in possession of this AMX Software shall not, and shall not
 *     permit any other person to, disclose, display, loan, publish, transfer
 *     (whether by sale, assignment, exchange, gift, operation of law or
 *     otherwise), license, sublicense, copy, or otherwise disseminate this
 *     AMX Software.
 * 
 *     This AMX Software is owned by AMX and is protected by United States
 *     copyright laws, patent laws, international treaty provisions, and/or
 *     state of Texas trade secret laws.
 * 
 *     Portions of this AMX Software may, from time to time, include
 *     pre-release code and such code may not be at the level of performance,
 *     compatibility and functionality of the final code. The pre-release code
 *     may not operate correctly and may be substantially modified prior to
 *     final release or certain features may not be generally released. AMX is
 *     not obligated to make or support any pre-release code. All pre-release
 *     code is provided "as is" with no warranties.
 * 
 *     This AMX Software is provided with restricted rights. Use, duplication,
 *     or disclosure by the Government is subject to restrictions as set forth
 *     in subparagraph (1)(ii) of The Rights in Technical Data and Computer
 *     Software clause at DFARS 252.227-7013 or subparagraphs (1) and (2) of
 *     the Commercial Computer Software Restricted Rights at 48 CFR 52.227-19,
 *     as applicable.
*/

PROGRAM_NAME='RMSModuleBase'
(*{{PS_SOURCE_INFO(PROGRAM STATS)                          *)
(***********************************************************)
(*  ORPHAN_FILE_PLATFORM: 1                                *)
(***********************************************************)
(*}}PS_SOURCE_INFO                                         *)
(***********************************************************)
#IF_NOT_DEFINED __RMS_MOD_BASE__
#DEFINE __RMS_MOD_BASE__

(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT

// Version For Code
CHAR __RMS_MOD_BASE_NAME__[]      = 'RMSModuleBase'
CHAR __RMS_MOD_BASE_VERSION__[]   = '3.3.33'

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

(***********************************************************)
(*                   INCLUDE FILES GO BELOW                *)
(***********************************************************)
#DEFINE RMS_DEV_MON_CALLBACK_PROXIES
#INCLUDE 'RMSCommon.axi'

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

// Debug
#IF_NOT_DEFINED bRMSDebug
VOLATILE CHAR bRMSDebug = 0
#END_IF

// Service
#IF_NOT_DEFINED bRMSService
VOLATILE CHAR bRMSService = 0
#END_IF

// Little version check hack...
#IF_NOT_DEFINED bRMSReportedVersion
VOLATILE CHAR bRMSReportedVersion
#END_IF //bRMSReportedVersion

// Ignore Communication Activity
#IF_NOT_DEFINED bRMSIgnoreComms
VOLATILE CHAR bRMSIgnoreComms = 1
#END_IF

// Ignore Power Status
// For starters, ignore power.  Only register power if
// we some indication of power status or discrete control
#IF_NOT_DEFINED bRMSIgnorePower
VOLATILE CHAR bRMSIgnorePower = 1
#END_IF

// Cannot control device
#IF_NOT_DEFINED bRMSEnablePowerFail
VOLATILE CHAR bRMSEnablePowerFail = 0
#END_IF

// Device is communicating
VOLATILE CHAR bRMSDevComms = 0

// Device power is on
VOLATILE CHAR bRMSDevPower = 0

// Cannot turn on power
VOLATILE CHAR bRMSDevPowerFail = 0

// Device information - Name, Manufacturer and Model
VOLATILE CHAR cRMSDeviceName[100]
VOLATILE CHAR cRMSDeviceMan[100]
VOLATILE CHAR cRMSDeviceModel[100]
VOLATILE INTEGER nRMSDeviceTimeout
VOLATILE CHAR cRMSDeviceIP[32]

(***********************************************************)
(*           SUBROUTINE DEFINITIONS GO BELOW               *)
(***********************************************************)

(********************************************)
(* Call Name: RMSDevMonRegisterCallbackProxy*)
(* Function:  My Version of Callback        *)
(* Param:     None                          *)
(* Return:    None                          *)
(* Note:      Caller must define this       *)
(********************************************)
DEFINE_FUNCTION RMSDevMonRegisterCallbackProxy()
{
  // First, register the device
  RMSRegisterDevAndParams()

  // Application Function - Caller Must Implement This Function...
  RMSDevMonRegisterCallback()
  RETURN;
}

(********************************************)
(* Call Name: RMSDevMonSetParamCallbackProxy*)
(* Function:  My Version of Callback        *)
(* Param:     None                          *)
(* Return:    None                          *)
(* Note:      Caller must define this       *)
(********************************************)
DEFINE_FUNCTION RMSDevMonSetParamCallbackProxy(DEV dvDPS, CHAR cName[], CHAR cValue[])
{
  // Application Function - Caller Must Implement This Function...
  RMSDevMonSetParamCallback(dvDPS, cName, cValue)
  RETURN;
}

(********************************************)
(* Call Name: RMSDevMonVersionCallbackProxy *)
(* Function:  My Version of Callback        *)
(* Param:     None                          *)
(* Return:    None                          *)
(* Note:      Caller must define this       *)
(********************************************)
#IF_DEFINED RMS_DEV_MON_VERSION_CALLBACK
DEFINE_FUNCTION RMSDevMonVersionCallbackProxy()
{
  // Application Function - Caller Must Implement This Function...
  OFF[bRMSReportedVersion]
  RMSDevMonVersionCallback()

  // Did they call my version check function?
  IF (!bRMSReportedVersion)
    RMSModuleBasePrintVersion()
  RETURN;
}
#END_IF // RMS_DEV_MON_VERSION_CALLBACK

(**************************************)
(* Call Name: RMSRegisterDevAndParams *)
(* Function:  Register Dev & Params   *)
(* Params:    None                    *)
(* Return:    None                    *)
(**************************************)
DEFINE_FUNCTION RMSRegisterDevAndParams()
{
  // Register Device and Parameters
  // Register device automatically registers online/offline
  RMSRegisterDevice(dvRealDevice, cRMSDeviceName, cRMSDeviceMan, cRMSDeviceModel)
  IF ((!bRMSIgnoreComms) && (nRMSDeviceTimeout > 0))
    RMSRegisterStockIndexParam(dvRealDevice, RMS_DEVICE_COMMS, "", RMS_TRACK_CHANGES, FALSE, RMS_COMP_EQUAL_TO, RMS_STAT_MAINTENANCE, RMS_PARAM_CANNOT_RESET, FALSE, RMS_PARAM_SET, bRMSDevComms, "RMS_DEVICE_STATUS_NO,'|',RMS_DEVICE_STATUS_YES")
  IF (!bRMSIgnorePower)
    RMSRegisterStockIndexParam(dvRealDevice, RMS_DEVICE_POWER, "", RMS_TRACK_CHANGES, TRUE, RMS_COMP_NONE, RMS_STAT_NOT_ASSIGNED, RMS_PARAM_CANNOT_RESET, FALSE, RMS_PARAM_SET, bRMSDevPower, "RMS_DEVICE_STATUS_OFF,'|',RMS_DEVICE_STATUS_ON")
  IF (bRMSEnablePowerFail)
    RMSRegisterStockIndexParam(dvRealDevice, RMS_DEVICE_CTRL_FAIL, "", RMS_TRACK_CHANGES, TRUE, RMS_COMP_EQUAL_TO, RMS_STAT_CONTROLSYSTEM_ERR, RMS_PARAM_CANNOT_RESET, FALSE, RMS_PARAM_SET, bRMSDevPowerFail, "RMS_DEVICE_CTRL_FAIL_PASS,'|',RMS_DEVICE_CTRL_FAIL_FAIL")
  IF (dvRealDevice.NUMBER == 0 && dvRealDevice.PORT <> 0)
    RMSRegisterStockStringParam(dvRealDevice, RMS_DEVICE_IP, "", RMS_DO_NOT_TRACK_CHANGES, "", RMS_COMP_NONE, RMS_STAT_NOT_ASSIGNED, RMS_PARAM_CANNOT_RESET, "", RMS_PARAM_SET, cRMSDeviceIP)
}

(**************************************)
(* Function: RMSDeviceSetPower        *)
(* Purpose:  Set Power State          *)
(**************************************)
DEFINE_FUNCTION RMSDeviceSetPower(CHAR bState, CHAR bChannel)
{
  // If we are ignoring it but we tried to use it, go ahead and register it now
  // And remember that we are trying to control the power
  // go away and store the state and register using that value
  IF (bRMSIgnorePower)
  {
    bRMSDevPower = (bState > 0)
    bRMSIgnorePower = FALSE
    RMSRegisterStockIndexParam(dvRealDevice, RMS_DEVICE_POWER, "", RMS_TRACK_CHANGES, TRUE, RMS_COMP_NONE, RMS_STAT_NOT_ASSIGNED, RMS_PARAM_CANNOT_RESET, FALSE, RMS_PARAM_SET, bRMSDevPower, "RMS_DEVICE_STATUS_OFF,'|',RMS_DEVICE_STATUS_ON")
  }

  // Now, see if the power has changed
  IF (bState <> bRMSDevPower)
  {
    bRMSDevPower = (bState > 0)
    IF (!bRMSIgnorePower)
      RMSChangeIndexParam(dvRealDevice, RMS_DEVICE_POWER, bRMSDevPower)
  }

  // Set wait for power failure
  IF (bRMSEnablePowerFail && bChannel)
  {
    // Start power failure wait
    CANCEL_WAIT 'RMSPowerFail'
    WAIT RMS_POWER_FAIL_TIMEOUT 'RMSPowerFail'
      RMSDeviceSetPowerFail(TRUE)
  }
}

(**************************************)
(* Function: RMSDeviceSetCommStatus   *)
(* Purpose:  Set Communication State  *)
(**************************************)
DEFINE_FUNCTION RMSDeviceSetCommStatus(CHAR bState)
{
  IF (bState <> bRMSDevComms)
  {
    bRMSDevComms = (bState > 0)
    IF ((!bRMSIgnoreComms) && (nRMSDeviceTimeout > 0))
      RMSChangeIndexParam(dvRealDevice, RMS_DEVICE_COMMS, bRMSDevComms)
  }
}

(**************************************)
(* Function: RMSDeviceSetPowerFail    *)
(* Purpose:  Set Power Fail           *)
(**************************************)
DEFINE_FUNCTION RMSDeviceSetPowerFail(CHAR bState)
{
  IF (bState <> bRMSDevPowerFail)
  {
    bRMSDevPowerFail = (bState > 0)
    IF (bRMSEnablePowerFail)
      RMSChangeIndexParam(dvRealDevice, RMS_DEVICE_CTRL_FAIL, bRMSDevPowerFail)
  }
}

(**************************************)
(* Function: RMSDeviceSetIP            *)
(* Purpose:  Set IP Address           *)
(**************************************)
DEFINE_FUNCTION RMSDeviceSetIP(CHAR cIP[])
{
  // Is this a real device or a socket?
  IF (dvRealDevice.NUMBER <> 0)
    RETURN;

  // Is this the master?
  IF (dvRealDevice.PORT < FIRST_LOCAL_PORT)
    RETURN;

  // If the IP does not match, set it
  IF (cIP <> cRMSDeviceIP)
  {
    cRMSDeviceIP = cIP
    RMSChangeStringParam(dvRealDevice, RMS_DEVICE_IP, RMS_PARAM_SET, cRMSDeviceIP)
  }
}


(**************************************)
(* Function: RMSModuleBasePrintVersion*)
(* Purpose:  Print version            *)
(**************************************)
DEFINE_FUNCTION RMSModuleBasePrintVersion()
{
  // What Version?
  SEND_STRING 0,"'  Running ',__RMS_MOD_BASE_NAME__,', v',__RMS_MOD_BASE_VERSION__"
  RMSDeviceMonitorPrintVersion()
  bRMSReportedVersion = TRUE
}

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

// What Version?
SEND_STRING 0,"'  Running ',__RMS_MOD_BASE_NAME__,', v',__RMS_MOD_BASE_VERSION__"

// Devices are unknown
cRMSDeviceName     = RMS_DEVICE_DEVICE
cRMSDeviceMan      = RMS_DEVICE_MAN
cRMSDeviceModel    = RMS_DEVICE_MODEL
nRMSDeviceTimeout  = RMS_DEVICE_TIMEOUT

(***********************************************************)
(*                THE EVENTS GOES BELOW                    *)
(***********************************************************)
DEFINE_EVENT

(*******************************************)
(* DATA: Engine Virtual Device             *)
(*******************************************)
DATA_EVENT[vdvRMSEngine]
{
  COMMAND :
  {
    STACK_VAR
    CHAR cUpperCmd[100]

    cUpperCmd = UPPER_STRING(DATA.TEXT)
    SELECT
    {
      //**********************************************************
      // Device Info
      // 'DEV INFO-DPS,Name,Manufacturer,Model'
      //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,9) = 'DEV INFO-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[9]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,9)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
        {
          cRMSDeviceName = RMSParseCommaSepString(cCmd)
          cRMSDeviceMan = RMSParseCommaSepString(cCmd)
          cRMSDeviceModel = RMSParseCommaSepString(cCmd)
					
	  // If online, re-register w/ new info
	  IF ([vdvRMSEngine,RMS_CH_SERVER_ONLINE])
	    RMSRegisterDevAndParams()
        }
      }
      //**********************************************************
      // Device Name
      // 'DEV NAME-DPS,Name'
     //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,9) = 'DEV NAME-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[9]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,9)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
				{
          cRMSDeviceName = RMSParseCommaSepString(cCmd)
					
					// If online, re-register w/ new info
					IF ([vdvRMSEngine,RMS_CH_SERVER_ONLINE])
					  RMSRegisterDevAndParams()
				}
					
      }
      //**********************************************************
      // Device Name
      // 'POWER FAIL ON-DPS'
      // 'PWRFAILON-DPS'
      // 'POWER FAIL OFF-DPS'
      // 'PWRFAILOFF-DPS'
      //**********************************************************
      ACTIVE ((LEFT_STRING(cUpperCmd,9) = 'PWRFAILON') ||
              (LEFT_STRING(cUpperCmd,13) = 'POWER FAIL ON')):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[9]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,9)
        cTrash = REMOVE_STRING(cCmd,'-',1)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
        {
          bRMSEnablePowerFail = TRUE
          IF ([vdvRMSEngine,RMS_CH_SERVER_ONLINE] && DEVICE_ID(dvRealDevice))
            RMSRegisterDeviceIndexParam(dvRealDevice, RMS_DEVICE_CTRL_FAIL, TRUE, RMS_COMP_EQUAL_TO, RMS_STAT_CONTROLSYSTEM_ERR, RMS_PARAM_CANNOT_RESET, FALSE, RMS_PARAM_SET, bRMSDevPowerFail, "RMS_DEVICE_CTRL_FAIL_PASS,'|',RMS_DEVICE_CTRL_FAIL_FAIL")
        }
      }
      ACTIVE ((LEFT_STRING(cUpperCmd,9) = 'PWRFAILOF') ||
              (LEFT_STRING(cUpperCmd,14) = 'POWER FAIL OFF')):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[9]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,9)
        cTrash = REMOVE_STRING(cCmd,'-',1)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
				{
					IF (bRMSEnablePowerFail == TRUE)
						RMSDeviceSetPowerFail(FALSE)
          bRMSEnablePowerFail = FALSE
				}
      }
      //**********************************************************
      // Device Info
      // 'COMM TO-DPS,Timeout'
      //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,8) = 'COMM TO-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[8]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,8)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
        {
	  // If disabling, make this parameter OK (i.e. communicating)
	  IF (ATOI(cCmd) = 0)
	    RMSDeviceSetCommStatus(TRUE)
          nRMSDeviceTimeout = ATOI(cCmd)
          CANCEL_WAIT 'RMSCommTo'
          IF (nRMSDeviceTimeout)
          {
            WAIT nRMSDeviceTimeout 'RMSCommTo'
	    { 	    
	      // if the Duet device communicating/initialized channel is ON, then do not 
	      // set  Comm Status to 'FALSE'
	      IF(![vdvDeviceModule,RMS_DUET_CH_DATA_INITIALIZED])  (* - added in v3.3 to support Duet VDV API *)
                RMSDeviceSetCommStatus(FALSE)
	    }
          }
        }
      }
      //**********************************************************
      // Device Power
      // 'DEV PWR-DPS,State'
      //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,8) = 'DEV PWR-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[8]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,8)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
        {
          // Set Power Status
          IF (ATOI(cCmd))
            RMSDeviceSetPower(TRUE,TRUE)
          ELSE
            RMSDeviceSetPower(FALSE,TRUE)
        }
      }
      //**********************************************************
      // Lamp Hours
      // 'LAMP HOURS-DPS,Value'
      //**********************************************************
      #IF_DEFINED RMS_LAMP_HOURS_CMD
      ACTIVE (LEFT_STRING(cUpperCmd,11) = 'LAMP HOURS-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[11]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,11)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
          RMSDevMonSetLampHours(ATOI(cCmd))
      }
      #END_IF
      //**********************************************************
      // Transport State
      // 'XPORT STATE-DPS,State'
      //**********************************************************
      #IF_DEFINED RMS_RUN_TIME_CMD
      ACTIVE (LEFT_STRING(cUpperCmd,12) = 'XPORT STATE-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[12]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,12)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
          RMSDevMonSetXport(ATOI(cCmd))
      }
      #END_IF
      //**********************************************************
      // Service
      // 'SERVICEON'
      // 'SERVICEOFF'
      // 'SERVICE-ON'
      // 'SERVICE-OFF'
      //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,7) = 'SERVICE'):
      {
        OFF[bRMSService]
        IF (FIND_STRING(cUpperCmd,'ON',1))
          ON[bRMSService]
      }
      //**********************************************************
      // Debug
      // 'DEBUGON'
      // 'DEBUGOFF'
      // 'DEBUG-ON'
      // 'DEBUG-OFF'
      //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,5) = 'DEBUG'):
      {
        OFF[bRMSDebug]
        IF (FIND_STRING(cUpperCmd,'ON',1))
          ON[bRMSDebug]
      }
    }
  }
}

(*******************************************)
(* DATA: Real Device                       *)
(*******************************************)
DATA_EVENT[dvRealDevice]
{
  // Online

  ONLINE :
  {
    // Register if device is not yet registerd
    // Register device automatically registers online/offline
    bRMSIgnoreComms = (!RMSDeviceIsSerial(dvRealDevice))
    RMSDevMonRegisterCallbackProxy()

    // If this device a socket?
    RMSDeviceSetIP(DATA.SOURCEIP)
  }

  // Offline
  OFFLINE :
  {
    //Set offline
    RMSNetLinxDeviceOffline(dvRealDevice)

    // If device is offline, we are not communicating with it!
    CANCEL_WAIT 'RMSCommTo'
    RMSDeviceSetCommStatus(FALSE)
  }

  // String coming in...
  STRING :
  {
    // Update value
    RMSDeviceSetCommStatus(TRUE)

    // If this device a socket?
    RMSDeviceSetIP(DATA.SOURCEIP)

    // If not ignoring comms, start a wait for communications timeout
    IF (!bRMSIgnoreComms)
    {
      CANCEL_WAIT 'RMSCommTo'
      IF (nRMSDeviceTimeout)
      {
        WAIT nRMSDeviceTimeout 'RMSCommTo'
        { 	    
	  // if the Duet device communicating/initialized channel is ON, then do not 
	  // set  Comm Status to 'FALSE'	  
	  IF(![vdvDeviceModule,RMS_DUET_CH_DATA_INITIALIZED])  (* - added in v3.3 to support Duet VDV API *)
            RMSDeviceSetCommStatus(FALSE)
	}
      }
    }
  }
}

(*******************************************)
(* DATA: Device Module/Device Driver       *)
(*******************************************)
DATA_EVENT[vdvDeviceModule]
{
  // Strings coming in...
  STRING:
  {
    // Power?
    IF (LEFT_STRING(UPPER_STRING(DATA.TEXT),6) = 'POWER=')
    {
      IF (FIND_STRING(UPPER_STRING(DATA.TEXT),'POWER=ON',1))
        RMSDeviceSetPower(1,FALSE)
      ELSE
      {
        GET_BUFFER_STRING(DATA.TEXT,6)
        RMSDeviceSetPower(ATOI(DATA.TEXT),FALSE)
      }
    }
    IF (LEFT_STRING(UPPER_STRING(DATA.TEXT),13) = 'MANUFACTURER=')
      cRMSDeviceMan = MID_STRING(DATA.TEXT,14,LENGTH_STRING(DATA.TEXT)-13)
    IF (LEFT_STRING(UPPER_STRING(DATA.TEXT),6) = 'MODEL=')
      cRMSDeviceModel = MID_STRING(DATA.TEXT,7,LENGTH_STRING(DATA.TEXT)-6)
  }
}

(*******************************************)
(* CHANNEL: Engine Virtual Device          *)
(*******************************************)
CHANNEL_EVENT[vdvDeviceModule,0]
{
  ON:
  {
    SWITCH (CHANNEL.CHANNEL)
    {
      // Power Toggle
      CASE RMS_CH_POWER_TOGGLE:
      {
        // Set wait for power failure
        IF (bRMSEnablePowerFail)
        {
          // Start power failure wait
          CANCEL_WAIT 'RMSPowerFail'
          WAIT RMS_POWER_FAIL_TIMEOUT 'RMSPowerFail'
            RMSDeviceSetPowerFail(TRUE)
        }
      }
      // Power On
      CASE RMS_CH_POWER_ON:
        RMSDeviceSetPower(TRUE,TRUE)
      // Power Off
      CASE RMS_CH_POWER_OFF:
        RMSDeviceSetPower(FALSE,TRUE)
      // Power Fail
      CASE RMS_CH_POWER_FAIL:
        RMSDeviceSetPowerFail(TRUE)
      // Power
      CASE RMS_CH_POWER_STATUS:
      {
	RMSDeviceSetPower(TRUE,TRUE)
        CANCEL_WAIT 'RMSPowerFail'
	IF (![vdvDeviceModule,RMS_CH_POWER_FAIL])
          RMSDeviceSetPowerFail(FALSE)
      }
    }
  }
  OFF:
  {
    SWITCH (CHANNEL.CHANNEL)
    {
      // Power Fail
      CASE RMS_CH_POWER_FAIL:
        RMSDeviceSetPowerFail(FALSE)
      // Power
      CASE RMS_CH_POWER_STATUS:
      {
				RMSDeviceSetPower(FALSE,TRUE)
        CANCEL_WAIT 'RMSPowerFail'
			  IF (![vdvDeviceModule,RMS_CH_POWER_FAIL])
          RMSDeviceSetPowerFail(FALSE)
      }
    }
  }
}



(*******************************************)
(* Channel: Duet virtual device            *)
(*          Duet Device Feedback Channel   *) 
(*          Communication is established   *)
(*          with device and device is      *)
(*          initialized while channel is   *)
(*          on.  (CH.252)                  *) 
(* - added in v3.3 to support Duet VDV API *)
(*******************************************)
CHANNEL_EVENT[vdvDeviceModule,RMS_DUET_CH_DATA_INITIALIZED]  // CH.252 
{
  ON:
  {
    // the Duet device is communicating
    CANCEL_WAIT 'RMSCommTo'
    RMSDeviceSetCommStatus(TRUE)
  }
  OFF:
  {
    // the Duet device is not communicating
    // If not ignoring comms, set the communicating parameter to 'False'
    IF (!bRMSIgnoreComms)
    {
      CANCEL_WAIT 'RMSCommTo'
      RMSDeviceSetCommStatus(FALSE)
    }
  }
}


(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

#END_IF //__RMS_MOD_BASE__
(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)