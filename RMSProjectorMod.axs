(*********************************************************************)
(*                                                                   *)
(*             AMX Resource Management Suite  (3.3.33)               *)
(*                                                                   *)
(*********************************************************************)
/*
 *  Legal Notice :
 * 
 *     Copyright, AMX LLC, 2009
 *
 *     Private, proprietary information, the sole property of AMX LLC.  The
 *     contents, ideas, and concepts expressed herein are not to be disclosed
 *     except within the confines of a confidential relationship and only
 *     then on a need to know basis.
 * 
 *     Any entity in possession of this AMX Software shall not, and shall not
 *     permit any other person to, disclose, display, loan, publish, transfer
 *     (whether by sale, assignment, exchange, gift, operation of law or
 *     otherwise), license, sublicense, copy, or otherwise disseminate this
 *     AMX Software.
 * 
 *     This AMX Software is owned by AMX and is protected by United States
 *     copyright laws, patent laws, international treaty provisions, and/or
 *     state of Texas trade secret laws.
 * 
 *     Portions of this AMX Software may, from time to time, include
 *     pre-release code and such code may not be at the level of performance,
 *     compatibility and functionality of the final code. The pre-release code
 *     may not operate correctly and may be substantially modified prior to
 *     final release or certain features may not be generally released. AMX is
 *     not obligated to make or support any pre-release code. All pre-release
 *     code is provided "as is" with no warranties.
 * 
 *     This AMX Software is provided with restricted rights. Use, duplication,
 *     or disclosure by the Government is subject to restrictions as set forth
 *     in subparagraph (1)(ii) of The Rights in Technical Data and Computer
 *     Software clause at DFARS 252.227-7013 or subparagraphs (1) and (2) of
 *     the Commercial Computer Software Restricted Rights at 48 CFR 52.227-19,
 *     as applicable.
*/

MODULE_NAME='RMSProjectorMod' (DEV     vdvDeviceModule,
                               DEV     dvRealDevice,
                               DEV     vdvRMSEngine)
(*{{PS_SOURCE_INFO(PROGRAM STATS)                          *)
(***********************************************************)
(*  ORPHAN_FILE_PLATFORM: 1                                *)
(***********************************************************)
(*}}PS_SOURCE_INFO                                         *)
(***********************************************************)
(***********************************************************)
(*          DEVICE NUMBER DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_DEVICE

(***********************************************************)
(*               CONSTANT DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_CONSTANT

// Version For Code
CHAR __RMS_PROJ_NAME__[]      = 'RMSProjectorMod'
CHAR __RMS_PROJ_VERSION__[]   = '3.3.33'

// Device Info
RMS_DEVICE_DEVICE[]          = 'Projector/Display'
RMS_DEVICE_MAN[]             = 'Unknown'
RMS_DEVICE_MODEL[]           = 'Unknown'

RMS_DEVICE_LAMP[]            = 'Lamp Hours'
RMS_DEVICE_STATUS_LIMIT      = 1800
RMS_DEVICE_STATUS_ZERO       = 0

RMS_LAMP_TIME_LOWER_BOUNDS   = 0 
RMS_LAMP_TIME_UPPER_BOUNDS   = 219000  // ~ 25 years 

(***********************************************************)
(*              DATA TYPE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_TYPE

(***********************************************************)
(*               VARIABLE DEFINITIONS GO BELOW             *)
(***********************************************************)
DEFINE_VARIABLE

// Get track of the remaining minute when I stop the timer
LONG lMinuteRemainder

// Track lamp hours
VOLATILE SLONG slLampHours

// Are we counting lamp hours ourselves?
VOLATILE CHAR bCountLampHours = 1

// Did we register lamp hours?
VOLATILE CHAR bIgnoreLampHours = 1

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

// What Version?
SEND_STRING 0,"'Running ',__RMS_PROJ_NAME__,', v',__RMS_PROJ_VERSION__"

(***********************************************************)
(*                   INCLUDE FILES GO BELOW                *)
(***********************************************************)
#DEFINE RMS_DEV_MON_VERSION_CALLBACK
#DEFINE RMS_LAMP_HOURS_CMD
#INCLUDE 'RMSModuleBase.axi'
#INCLUDE 'RMSTimer'

(***********************************************************)
(*           SUBROUTINE DEFINITIONS GO BELOW               *)
(***********************************************************)

(***************************************)
(* Call Name: RMSDevMonRegisterCallback*)
(* Function:  time to register devices *)
(* Param:     None                     *)
(* Return:    None                     *)
(* Note:      Caller must define this  *)
(***************************************)
DEFINE_FUNCTION RMSDevMonRegisterCallback()
{
  // Device Base Handles Device Reg, Online, Comms and Power
  // If we are not passed lamp hours, set it up as resetable with unchanged initial value
  // Else If we are passed lamp hours, set it up as no reset and set inital value
  // If we do not know the lamp hour (yet), leave it as unchanged.
  SELECT
  {
    ACTIVE (bIgnoreLampHours): {}
    ACTIVE (bCountLampHours):
      RMSRegisterStockNumberParam(dvRealDevice, RMS_DEVICE_LAMP, RMS_UNIT_HOURS, RMS_TRACK_CHANGES, RMS_DEVICE_STATUS_LIMIT, RMS_COMP_GREATER_THAN, RMS_STAT_MAINTENANCE, RMS_PARAM_CAN_RESET, RMS_DEVICE_STATUS_ZERO, RMS_PARAM_UNCHANGED, RMS_DEVICE_STATUS_ZERO, 0, 0)
    ACTIVE (slLampHours > 0):
      RMSRegisterStockNumberParam(dvRealDevice, RMS_DEVICE_LAMP, RMS_UNIT_HOURS, RMS_TRACK_CHANGES, RMS_DEVICE_STATUS_LIMIT, RMS_COMP_GREATER_THAN, RMS_STAT_MAINTENANCE, RMS_PARAM_CANNOT_RESET, RMS_DEVICE_STATUS_ZERO, RMS_PARAM_SET, slLampHours, 0, 0)
    ACTIVE (1):
      RMSRegisterStockNumberParam(dvRealDevice, RMS_DEVICE_LAMP, RMS_UNIT_HOURS, RMS_TRACK_CHANGES, RMS_DEVICE_STATUS_LIMIT, RMS_COMP_GREATER_THAN, RMS_STAT_MAINTENANCE, RMS_PARAM_CANNOT_RESET, RMS_DEVICE_STATUS_ZERO, RMS_PARAM_UNCHANGED, RMS_DEVICE_STATUS_ZERO, 0, 0)
  }
}

(***************************************)
(* Call Name: RMSDevMonSetParamCallback*)
(* Function:  Reset parameters         *)
(* Param:     DPS, Name, Value         *)
(* Return:    None                     *)
(* Note:      Caller must define this  *)
(***************************************)
DEFINE_FUNCTION RMSDevMonSetParamCallback(DEV dvDPS, CHAR cName[], CHAR cValue[])
STACK_VAR CHAR bRunning
{
  // See if this is ours
  IF (dvDPS == dvRealDevice)
  {
    // This is ours and are we counting lamp hours?
    IF (cName == RMS_DEVICE_LAMP && bCountLampHours == TRUE)
    {
      bRunning = RMSTimerIsRunning()
      RMSTimerStop()
      lMinuteRemainder  = 0
      IF (bRunning)
        RMSTimerStart()
    }
  }
}

(***************************************)
(* Call Name: RMSDevMonVersionCallback *)
(* Function:  Time to print version    *)
(* Param:     None                     *)
(* Return:    None                     *)
(* Note:      Caller must define this  *)
(***************************************)
DEFINE_FUNCTION RMSDevMonVersionCallback()
{
  // What Version?
  SEND_STRING 0,"'Running ',__RMS_PROJ_NAME__,', v',__RMS_PROJ_VERSION__"
  RMSModuleBasePrintVersion()
  RMSTimerPrintVersion()
}

(**************************************)
(* Call Name: RMSTimerCallback        *)
(* Function:  Timer Callback          *)
(* Param:     None                    *)
(* Return:    None                    *)
(**************************************)
DEFINE_FUNCTION RMSTimerCallback()
STACK_VAR
LONG lValue
LOCAL_VAR
SLONG slAccum
{
  // Are we counting?
  IF (bCountLampHours == FALSE)
    RETURN;

  // This fires everytime there is a minute change on the projector
  lValue = RMSTimerGetMinutes()
  IF ((lValue % 60) == 0)
  {
    // Accumulate this
    slAccum++

    // Make sure RMS is online.  Otherwise, continue to accumulate
    IF ((![vdvRMSEngine,RMS_CH_SERVER_ONLINE]) || (bRMSService))
      RETURN;

    // Report value
    RMSChangeNumberParam(dvRealDevice, RMS_DEVICE_LAMP, RMS_PARAM_INC, slAccum)
    slAccum = 0
  }
}

(***************************************)
(* Call Name: RMSDevMonSetLampPower    *)
(* Function:  Set Lamp Power           *)
(* Param:     1=On, 0=Off              *)
(* Return:    None                     *)
(***************************************)
DEFINE_FUNCTION RMSDevMonSetLampPower(CHAR bState)
{
  // Are we counting?  if not, who cares?  This is for lamp couting only
  // DeviceVase.axi takes care of power
  IF (bCountLampHours == FALSE)
    RETURN;

  // Did we register yet?  If not, do it now
  IF (bIgnoreLampHours)
  {
    bIgnoreLampHours = FALSE
    RMSDevMonRegisterCallback()
  }

  // Did the state change
  IF (bState > 0)
  {
    // Start Timer & offset timer to think it started lMinuteRemainder minutes before now
    RMSTimerStart()
    RMSTimerResetStartTime(0,TYPE_CAST(0-lMinuteRemainder),0)
  }
  ELSE
  {
    // Remember minute mark and stop timer
    IF (RMSTimerIsRunning())
		{
      lMinuteRemainder = RMSTimerGetMinutes()
			lMinuteRemainder = lMinuteRemainder % 60
		}
    RMSTimerStop();
  }
}

(***************************************)
(* Call Name: RMSDevMonSetLampHours    *)
(* Function:  Set Lamp Hours           *)
(* Param:     # of Hours               *)
(* Return:    None                     *)
(***************************************)
DEFINE_FUNCTION RMSDevMonSetLampHours(SLONG slHours)
{
    // Let the comm module handle lamp hours
    // Also, kill timer, we will not be using it anymore...
    bCountLampHours = FALSE
    RMSTimerStop();

    // Did we register yet?  If not, do it now
    IF (bIgnoreLampHours)
    {
      bIgnoreLampHours = FALSE
      RMSDevMonRegisterCallback()
    }

    // bounds check the lamp hours value
    // note if erroneous lamp time is received from Duet module, it will be a Duet min int value (-2147483648)
    IF((slHours >= RMS_LAMP_TIME_LOWER_BOUNDS) && (slHours <= RMS_LAMP_TIME_UPPER_BOUNDS))
    {
      // Is this a change?
      IF (slHours <> slLampHours)
      {
        slLampHours = slHours
        RMSChangeNumberParam(dvRealDevice, RMS_DEVICE_LAMP, RMS_PARAM_SET, slLampHours)
      }
    }
}

(***********************************************************)
(*                STARTUP CODE GOES BELOW                  *)
(***********************************************************)
DEFINE_START

// Setup timer
RMSTimerSetCallbackTime(RMS_TIMER_CB_MINUTE)

(***********************************************************)
(*                THE EVENTS GOES BELOW                    *)
(***********************************************************)
DEFINE_EVENT

(*******************************************)
(* DATA: Projector Module                  *)
(*******************************************)
DATA_EVENT[vdvDeviceModule]
{
  // String coming in...
  STRING :
  {
    IF (LEFT_STRING(UPPER_STRING(DATA.TEXT),6) = 'POWER=')
    {
      IF (FIND_STRING(UPPER_STRING(DATA.TEXT),'POWER=ON',1))
        RMSDevMonSetLampPower(TRUE)
      ELSE
      {
        GET_BUFFER_STRING(DATA.TEXT,6)
        RMSDevMonSetLampPower(ATOI(DATA.TEXT))
      }
    }
    IF (LEFT_STRING(UPPER_STRING(DATA.TEXT),9) = 'LAMPTIME=')
    {
      GET_BUFFER_STRING(DATA.TEXT,9)
      RMSDevMonSetLampHours(ATOI(DATA.TEXT))
    }
  }
  
  // Duet command coming in...
  // (* - added in v3.3 to support Duet VDV API *)
  COMMAND :
  {
    // local variables
    STACK_VAR CHAR    cCmd[RMS_DUET_MAX_CMD_LEN]
    STACK_VAR CHAR    cHeader[RMS_DUET_MAX_HDR_LEN]
    STACK_VAR CHAR    cParameter[RMS_DUET_MAX_PARAM_LEN]
    STACK_VAR INTEGER nParameter

    // get received SNAPI command
    cCmd = DATA.TEXT

    // parse command header
    cHeader = RMSDuetParseCmdHeader(cCmd)
    SWITCH (cHeader)
    {
        // SNAPI::LAMPTIME-<time>
        CASE 'LAMPTIME' :
        {
           // send lamp hour value to RMS
	   RMSDevMonSetLampHours(ATOI(RMSDuetParseCmdParam(cCmd)))
        }
    }
  }
}


(*******************************************)
(* CHANNEL: Projector Module               *)
(*******************************************)
CHANNEL_EVENT[vdvDeviceModule,0]
{
  ON:
  {
    SWITCH (CHANNEL.CHANNEL)
    {
      // Power On
      CASE RMS_CH_POWER_TOGGLE: {}
      // Power On
      CASE RMS_CH_POWER_ON:
        RMSDevMonSetLampPower(TRUE)
      // Power Off
      CASE RMS_CH_POWER_OFF:
        RMSDevMonSetLampPower(FALSE)
      // Power
      CASE RMS_CH_POWER_STATUS:
        RMSDevMonSetLampPower(TRUE)
    }
  }
  OFF:
  {
    SWITCH (CHANNEL.CHANNEL)
    {
      // Power
      CASE RMS_CH_POWER_STATUS:
        RMSDevMonSetLampPower(FALSE)
    }
  }
}

(*******************************************)
(* DATA: Engine Virtual Device             *)
(*******************************************)
DATA_EVENT[vdvRMSEngine]
{
  COMMAND :
  {
    STACK_VAR
    CHAR cUpperCmd[100]

    cUpperCmd = UPPER_STRING(DATA.TEXT)
    SELECT
    {
      //**********************************************************
      // Device Power
      // 'DEV PWR-DPS,State'
      //**********************************************************
      ACTIVE (LEFT_STRING(cUpperCmd,8) = 'DEV PWR-'):
      {
        // Get Params
        STACK_VAR
        CHAR cCmd[500]
        CHAR cTrash[8]
        DEV  dvDPS

        cCmd = DATA.TEXT
        cTrash = GET_BUFFER_STRING(cCmd,8)
        RMSParseDPSFromString(RMSParseCommaSepString(cCmd),dvDPS)
        IF (dvDPS == dvRealDevice)
        {
          // Set Power Status
          IF (ATOI(cCmd))
            RMSDevMonSetLampPower(TRUE)
          ELSE
            RMSDevMonSetLampPower(FALSE)
        }
      }
    }
  }
}

(***********************************************************)
(*            THE ACTUAL PROGRAM GOES BELOW                *)
(***********************************************************)
DEFINE_PROGRAM

(***********************************************************)
(*                     END OF PROGRAM                      *)
(*        DO NOT PUT ANY CODE BELOW THIS COMMENT           *)
(***********************************************************)